﻿using System.Data.Entity;
using NeuralNetworksResults.Models;
using NeuralNetworksResults.ModelsConfiguration;

namespace NeuralNetworksResults.Context
{
    public class NeuralNetworksResultDBContext : DbContext
    {
        public NeuralNetworksResultDBContext() : base("name=ResearchNeuralNetworkEntities")
        {
            Database.SetInitializer( new MigrateDatabaseToLatestVersion<NeuralNetworksResultDBContext, Migrations.Configuration>("ResearchNeuralNetworkEntities") );
        }

        /// <summary>
        /// Gets or sets the researches.
        /// </summary>
        /// <value>
        /// The researches.
        /// </value>
        public DbSet<Research> Researches { get; set; }

        /// <summary>
        /// Gets or sets the pc infos.
        /// </summary>
        /// <value>
        /// The pc infos.
        /// </value>
        public DbSet<PCInfo> PCInfos { get; set; }

        /// <summary>
        /// Gets or sets the test results.
        /// </summary>
        /// <value>
        /// The test results.
        /// </value>
        public DbSet<TestResult> TestResults { get; set; }

        /// <summary>
        /// Gets or sets the attempt results.
        /// </summary>
        /// <value>
        /// The attempt results.
        /// </value>
        public DbSet<AttemptResult> AttemptResults { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating( modelBuilder );

            modelBuilder.Configurations.Add( new AttemptResultConfiguration() );
            modelBuilder.Configurations.Add( new PCInfoConfiguration() );
            modelBuilder.Configurations.Add( new ResearchConfiguration() );
            modelBuilder.Configurations.Add( new TestResultConfiguration() );
        }
    }
}