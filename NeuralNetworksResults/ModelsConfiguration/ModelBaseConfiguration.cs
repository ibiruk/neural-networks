﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using NeuralNetworksResults.Models;

namespace NeuralNetworksResults.ModelsConfiguration
{
    public class ModelBaseConfiguration<T> : EntityTypeConfiguration<T> where T :  ModelBase
    {
        public ModelBaseConfiguration()
        {
            this.Property(c => c.Id).IsRequired()
                .HasDatabaseGeneratedOption( DatabaseGeneratedOption.Identity )
                .IsRequired();

            this.Property(x => x.InsertDT)
                .IsRequired();
        }

    }
}
