﻿using System;

namespace NeuralNetworksResults.Models
{
    public class ModelBase : IModelBase
    {
        public ModelBase()
        {
            InsertDT = DateTime.UtcNow;
        }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets the insert dt.
        /// </summary>
        /// <value>
        /// The insert dt.
        /// </value>
        public DateTime InsertDT { get; set; }
    }
}
