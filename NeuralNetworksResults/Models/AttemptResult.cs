﻿namespace NeuralNetworksResults.Models
{
    public class AttemptResult: ModelBase
    {
        /// <summary>
        /// Gets or sets the number of test.
        /// </summary>
        /// <value>
        /// The number of test.
        /// </value>
        public int NumberOfTest { get; set; }

        /// <summary>
        /// Gets or sets the time to send message.
        /// </summary>
        /// <value>
        /// The time to send message.
        /// </value>
        public decimal TimeToSendMessage { get; set; }

        /// <summary>
        /// Gets or sets the time to recive message.
        /// </summary>
        /// <value>
        /// The time to recive message.
        /// </value>
        public decimal TimeToReciveMessage { get; set; }

        /// <summary>
        /// Gets or sets the time to calculate weight.
        /// </summary>
        /// <value>
        /// The time to calculate weight.
        /// </value>
        public decimal TimeToCalculateWeight { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is output mach.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is output mach; otherwise, <c>false</c>.
        /// </value>
        public bool IsOutputMach { get; set; }

        /// <summary>
        /// Gets or sets the elipsated time.
        /// </summary>
        /// <value>
        /// The elipsated time.
        /// </value>
        public decimal ElipsatedTime { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is weight mach.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is weight mach; otherwise, <c>false</c>.
        /// </value>
        public bool IsWeightMach { get; set; }

        /// <summary>
        /// Gets or sets the inputs.
        /// </summary>
        /// <value>
        /// The inputs.
        /// </value>
        public string Inputs { get; set; }

        /// <summary>
        /// Gets or sets the output.
        /// </summary>
        /// <value>
        /// The output.
        /// </value>
        public string Output { get; set; }

        /// <summary>
        /// Gets or sets the test result identifier.
        /// </summary>
        /// <value>
        /// The test result identifier.
        /// </value>
        public int TestResultId { get; set; }

        /// <summary>
        /// Gets or sets the test result.
        /// </summary>
        /// <value>
        /// The test result.
        /// </value>
        public virtual TestResult TestResult { get; set; }

    }
}
