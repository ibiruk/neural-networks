﻿using System;
using System.Collections.Generic;
using NeuralNetworksInfrastructure.Enum;

namespace NeuralNetworksResults.Models
{
    public class Research: ModelBase
    {
        /// <summary>
        /// Gets or sets the on date.
        /// </summary>
        /// <value>
        /// The on date.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the count of perceptrons.
        /// </summary>
        /// <value>
        /// The count of perceptron.
        /// </value>
        public int CountOfPerceptrons { get; set; }

        /// <summary>
        /// Gets or sets the count of perceptrons nodes.
        /// </summary>
        /// <value>
        /// The count of percepton nodes.
        /// </value>
        public int CountOfPerceptonNodes { get; set; }

        /// <summary>
        /// Gets or sets the weight limit.
        /// </summary>
        /// <value>
        /// The weight limit.
        /// </value>
        public int WeightLimit { get; set; }

        /// <summary>
        /// Gets or sets the type of values.
        /// </summary>
        /// <value>
        /// The type of values.
        /// </value>
        public ETypesOfTPM TypeOfValues { get; set; }

        /// <summary>
        /// Gets or sets the pc infos.
        /// </summary>
        /// <value>
        /// The pc infos.
        /// </value>
        public virtual ICollection<PCInfo> PCInfos { get; set; }

        /// <summary>
        /// Gets or sets the test results.
        /// </summary>
        /// <value>
        /// The test results.
        /// </value>
        public virtual ICollection<TestResult> TestResults { get; set; }
    }
}
