﻿using System;

namespace NeuralNetworksResults.Models
{
    public interface IModelBase
    {
        int Id { get; set; }
        DateTime InsertDT { get; set; }
    }
}