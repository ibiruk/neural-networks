﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic.Devices;
using NeuralNetworksInfrastructure.Enum;

namespace NeuralNetworksResults.Models
{
    public class PCInfo: ModelBase
    {
        /// <summary>
        ///     Gets or sets the os version.
        /// </summary>
        /// <value>
        ///     The os version.
        /// </value>
        public string OS { get; set; }

        /// <summary>
        ///     Gets or sets the processor count.
        /// </summary>
        /// <value>
        ///     The processor count.
        /// </value>
        public int ProcessorCount { get; set; }

        /// <summary>
        ///     Gets or sets the total physical memory.
        /// </summary>
        /// <value>
        ///     The total physical memory.
        /// </value>
        public ulong TotalPhysicalMemory { get; set; }

        /// <summary>
        ///     Gets or sets the name of the processor.
        /// </summary>
        /// <value>
        ///     The name of the processor.
        /// </value>
        public string Processor { get; set; }

        /// <summary>
        ///     Gets or sets the type of the application.
        /// </summary>
        /// <value>
        ///     The type of the application.
        /// </value>
        public EAppType AppType { get; set; }

        /// <summary>
        /// Gets or sets the research identifier.
        /// </summary>
        /// <value>
        /// The research identifier.
        /// </value>
        public int ResearchId { get; set; }

        /// <summary>
        ///     Gets or sets the research.
        /// </summary>
        /// <value>
        ///     The research.
        /// </value>
        public virtual Research Research { get; set; }

        /// <summary>
        /// Gets or sets the test results
        /// .
        /// </summary>
        /// <value>
        /// The test results.
        /// </value>
        public virtual ICollection<TestResult> TestResults { get; set; }


        public static PCInfo GetCurrentPCInfo()
        {
            var info = new PCInfo();
            var compInfo = new ComputerInfo();
            

            info.OS = compInfo.OSFullName;
            info.ProcessorCount = Environment.ProcessorCount;
            info.Processor = Environment.GetEnvironmentVariable( "PROCESSOR_IDENTIFIER" );
            info.TotalPhysicalMemory = compInfo.TotalPhysicalMemory;

            return info;
        }

    }
}