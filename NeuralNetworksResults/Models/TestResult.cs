﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NeuralNetworksResults.Models
{
    public class TestResult: ModelBase
    {
        /// <summary>
        /// Gets or sets the number of test.
        /// </summary>
        /// <value>
        /// The number of test.
        /// </value>
        public int NumberOfTest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is synchronize succeed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is synchronize succeed; otherwise, <c>false</c>.
        /// </value>
        public bool IsSyncSucceed { get; set; }

        /// <summary>
        /// Gets or sets the elipsated time.
        /// </summary>
        /// <value>
        /// The elipsated time.
        /// </value>
        public decimal ElipsatedTime { get; set; }

        /// <summary>
        /// Gets or sets the synchronize operations count.
        /// </summary>
        /// <value>
        /// The synchronize operations count.
        /// </value>
        public int SyncOperationsCount { get; set; }

        /// <summary>
        /// Gets or sets the result weights.
        /// </summary>
        /// <value>
        /// The result weights.
        /// </value>
        public string ResultWeights { get; set; }


        /// <summary>
        /// Gets or sets the attempts count.
        /// </summary>
        /// <value>
        /// The attempts count.
        /// </value>
        public int AttemptsCount { get; set; }

        /// <summary>
        /// Gets or sets the research.
        /// </summary>
        /// <value>
        /// The research.
        /// </value>
        public int ResearchId { get; set; }

        /// <summary>
        /// Gets or sets the research.
        /// </summary>
        /// <value>
        /// The research.
        /// </value>
        public virtual Research Research { get; set; }
        
        /// <summary>
        /// Gets or sets the attempt result.
        /// </summary>
        /// <value>
        /// The attempt result.
        /// </value>
        public virtual ICollection<AttemptResult> AttemptResult { get; set; }

    }
}
