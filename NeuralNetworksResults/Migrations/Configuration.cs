namespace NeuralNetworksResults.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<NeuralNetworksResults.Context.NeuralNetworksResultDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;

            CommandTimeout = Int32.MaxValue;
            

        }

        protected override void Seed(NeuralNetworksResults.Context.NeuralNetworksResultDBContext context)
        {

        }
    }
}
