// <auto-generated />
namespace NeuralNetworksResults.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddAttemptsCount : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddAttemptsCount));
        
        string IMigrationMetadata.Id
        {
            get { return "201612191843497_AddAttemptsCount"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
