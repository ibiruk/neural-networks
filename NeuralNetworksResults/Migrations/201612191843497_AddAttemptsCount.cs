namespace NeuralNetworksResults.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAttemptsCount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestResults", "AttemptsCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestResults", "AttemptsCount");
        }
    }
}
