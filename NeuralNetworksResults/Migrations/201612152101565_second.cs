namespace NeuralNetworksResults.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AttemptResults", "TestResult_Id", "dbo.TestResults");
            DropForeignKey("dbo.TestResults", "Research_Id", "dbo.Researches");
            DropForeignKey("dbo.PCInfoes", "Research_Id", "dbo.Researches");
            DropIndex("dbo.AttemptResults", new[] { "TestResult_Id" });
            DropIndex("dbo.TestResults", new[] { "Research_Id" });
            DropIndex("dbo.PCInfoes", new[] { "Research_Id" });
            RenameColumn(table: "dbo.AttemptResults", name: "TestResult_Id", newName: "TestResultId");
            RenameColumn(table: "dbo.TestResults", name: "Research_Id", newName: "ResearchId");
            RenameColumn(table: "dbo.PCInfoes", name: "Research_Id", newName: "ResearchId");
            AlterColumn("dbo.AttemptResults", "TestResultId", c => c.Int(nullable: false));
            AlterColumn("dbo.TestResults", "ResearchId", c => c.Int(nullable: false));
            AlterColumn("dbo.PCInfoes", "ResearchId", c => c.Int(nullable: false));
            CreateIndex("dbo.AttemptResults", "TestResultId");
            CreateIndex("dbo.TestResults", "ResearchId");
            CreateIndex("dbo.PCInfoes", "ResearchId");
            AddForeignKey("dbo.AttemptResults", "TestResultId", "dbo.TestResults", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TestResults", "ResearchId", "dbo.Researches", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PCInfoes", "ResearchId", "dbo.Researches", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PCInfoes", "ResearchId", "dbo.Researches");
            DropForeignKey("dbo.TestResults", "ResearchId", "dbo.Researches");
            DropForeignKey("dbo.AttemptResults", "TestResultId", "dbo.TestResults");
            DropIndex("dbo.PCInfoes", new[] { "ResearchId" });
            DropIndex("dbo.TestResults", new[] { "ResearchId" });
            DropIndex("dbo.AttemptResults", new[] { "TestResultId" });
            AlterColumn("dbo.PCInfoes", "ResearchId", c => c.Int());
            AlterColumn("dbo.TestResults", "ResearchId", c => c.Int());
            AlterColumn("dbo.AttemptResults", "TestResultId", c => c.Int());
            RenameColumn(table: "dbo.PCInfoes", name: "ResearchId", newName: "Research_Id");
            RenameColumn(table: "dbo.TestResults", name: "ResearchId", newName: "Research_Id");
            RenameColumn(table: "dbo.AttemptResults", name: "TestResultId", newName: "TestResult_Id");
            CreateIndex("dbo.PCInfoes", "Research_Id");
            CreateIndex("dbo.TestResults", "Research_Id");
            CreateIndex("dbo.AttemptResults", "TestResult_Id");
            AddForeignKey("dbo.PCInfoes", "Research_Id", "dbo.Researches", "Id");
            AddForeignKey("dbo.TestResults", "Research_Id", "dbo.Researches", "Id");
            AddForeignKey("dbo.AttemptResults", "TestResult_Id", "dbo.TestResults", "Id");
        }
    }
}
