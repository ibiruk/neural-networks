﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using log4net;
using NeuralNetworksResults.Models;

namespace NeuralNetworksResults.NeuralNetworksResult
{
    public class DataBaseHelper<TDb> where TDb : DbContext, new()
    {

        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public async Task<TM> AddOrUpdateItem<TM>(TM item) where TM : class, IModelBase 
        {
            Log.Info("AddOrUpdateItem Started");
            using ( var context = new TDb() )
            {
                var existed = ExecuteQuery((con, it) =>con.Set<TM>().Find(it.Id ), item );
                var entry = context.Entry(item);
                entry.State = existed != null ? EntityState.Modified : EntityState.Added;
                await context.SaveChangesAsync();
                Log.Info("AddOrUpdateItem Ended");
                return entry.Entity;
            }
        }

        public T ExecuteQuery<TY, T>(Func<TDb, TY, T> function, TY arg) 
        {
            Log.Info("ExecuteQuery Started");
            using ( var context = new TDb() )
            {
                var r =  function( context, arg );
                Log.Info("ExecuteQuery Ended");
                return r;
            }
        }

        public async Task<T>  ExecuteQuery<TY, T>(Func<TDb, TY, Task<T>> function, TY arg) 
        {
            Log.Info(" async ExecuteQuery Started");
            using (var context = new TDb())
            {
                var r =  await function(context, arg);
                Log.Info(" asyncExecuteQuery Ended");
                return r;
            }
        }
    }
}
