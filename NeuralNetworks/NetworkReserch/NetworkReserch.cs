﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NeuralNetworks.NeuralNetwork;
using NeuralNetworksInfrastructure.Enum;
using NeuralNetworksInfrastructure.EventArgs;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;
using NeuralNetworksInfrastructure.Reports;
using NeuralNetworksResults.Context;
using NeuralNetworksResults.Models;
using NeuralNetworksResults.NeuralNetworksResult;

namespace NeuralNetworks.NetworkReserch
{
    public sealed class NetworkReserch : IResearch, IDisposable
    {
        #region Fields

        private bool _isDisposed;
        private SocketPermission _mySocketPermission1 = new SocketPermission(PermissionState.Unrestricted);
        private readonly DataBaseHelper<NeuralNetworksResultDBContext> dbHelper;
        private Random _random;
        private bool _stop { get; set; }
        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the number of tests.
        /// </summary>
        /// <value>
        ///     The number of tests.
        /// </value>
        public int NumberOfTests { get; private set; }

        /// <summary>
        ///     Gets or sets the limit of attempts.
        /// </summary>
        /// <value>
        ///     The limit of attempts.
        /// </value>
        public int LimitOfAttempts { get; private set; }

        /// <summary>
        ///     Gets or sets the network configuration.
        /// </summary>
        /// <value>
        ///     The network configuration.
        /// </value>
        public NetworkConfig NetworkConfiguration { get; private set; }

        /// <summary>
        ///     Gets or sets the type of the application.
        /// </summary>
        /// <value>
        ///     The type of the application.
        /// </value>
        public EAppType ApplicationType { get; private set; }

        /// <summary>
        /// Gets or sets the reports.
        /// </summary>
        /// <value>
        /// The reports.
        /// </value>
        public List<Report> Reports { get; private set; }

        /// <summary>
        ///     Gets or sets the research.
        /// </summary>
        /// <value>
        ///     The research.
        /// </value>
        public Research ResearchEntity { get; private set; }

        /// <summary>
        ///     Gets or sets the socket worker.
        /// </summary>
        /// <value>
        ///     The socket worker.
        /// </value>
        private SocketWorker SocketWorker { get; set; }
        
        #endregion

        #region  Contructors

        /// <summary>
        /// Prevents a default instance of the <see cref="Research"/> class from being created.
        /// </summary>
        private NetworkReserch()
        {
            dbHelper = new DataBaseHelper<NeuralNetworksResultDBContext>();
            Reports = new List<Report>();
            _stop = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Research"/> class.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <param name="port">The port.</param>
        /// <param name="appType">Type of the application.</param>
        public NetworkReserch(string host, int port, EAppType appType) : this()
        {
            SocketWorker = new SocketWorker(host, port, appType);
            SocketWorker.Created += SocketWorkerOnCreated;
            SocketWorker.Connected += SocketWorkerOnConnected;
            SocketWorker.Error += SocketWorkerOnError;
            ApplicationType = appType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Research"/> class.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <param name="port">The port.</param>
        /// <param name="limitOfAttempts">The limit of attempts.</param>
        /// <param name="numberOfTest">The number of test.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="appType">Type of the application.</param>
        public NetworkReserch(string host, int port, int limitOfAttempts, int numberOfTest,
            NetworkConfig configuration, EAppType appType) : this(host, port, appType)
        {
            LimitOfAttempts = limitOfAttempts;
            NumberOfTests = numberOfTest;
            NetworkConfiguration = configuration;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets a value indicating whether [if research stoped].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [if research stoped]; otherwise, <c>false</c>.
        /// </value>
        private async Task<bool> IfResearchStoped()
        {
            var isStop = string.Empty;
            var stopStr = _stop
                ? ESocketAnswer.Yes.ToString()
                : ESocketAnswer.No.ToString();
            if (ApplicationType == EAppType.Server)
            {
                await SendInfoToClientAndValidateResult(stopStr);

                isStop = await SocketWorker.GetMessageAsync();
            }
            else
            {
                isStop = await SocketWorker.GetMessageAsync();
                await SendInfoToClientAndValidateResult(stopStr);

            }

            return isStop.Equals(ESocketAnswer.Yes.ToString()) || _stop;
        }
        
        /// <summary>
        /// Creates the socket.
        /// </summary>
        public void CreateSocket()
        {
            SocketWorker.CreateSocket();
        }

        /// <summary>
        /// Creates the socket.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DoWorkEventArgs"/> instance containing the event data.</param>
        public void CreateSocket(object sender, DoWorkEventArgs e)
        {
            CreateSocket();
        }

        /// <summary>
        /// Synchronizes the parameters.
        /// </summary>
        private async Task SyncParams()
        {
            int seed;
            if ( ApplicationType == EAppType.Server )
            {
                await SendInfoToClientAndValidateResult(NetworkConfiguration.ToString(false));
                seed = new CryptoRandom().Next();
                _random = new Random(seed);
                await SendInfoToClientAndValidateResult(seed.ToString());
                await SendInfoToClientAndValidateResult(LimitOfAttempts.ToString());
                await SendInfoToClientAndValidateResult(NumberOfTests.ToString());

            }
            else
            {
                NetworkConfiguration = new NetworkConfig( await SocketWorker.GetMessageAsync() );
                await SendYes();
                seed = int.Parse( await SocketWorker.GetMessageAsync() );
                _random = new Random( seed );
                await SendYes();
                LimitOfAttempts = int.Parse( await SocketWorker.GetMessageAsync() );
                await SendYes();
                NumberOfTests = int.Parse( await SocketWorker.GetMessageAsync() );
                await SendYes();
            }

            ResearchEntity = await GetResearchEntity();
        }

        private async Task SendYes()
        {
            await SocketWorker.SendMessageAsync(ESocketAnswer.Yes.ToString());
        }

        /// <summary>
        /// Sends the information to client and validate result.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="resultForCheck">The result for check.</param>
        /// <returns></returns>
        /// <exception cref="VerificationException"></exception>
        private async Task SendInfoToClientAndValidateResult(string str, string resultForCheck = "")
        {
            Log.Info($"SendInfoToClientAndValidateResult Started {str}");
            await SocketWorker.SendMessageAsync( str );
            var result = await SocketWorker.GetMessageAsync();
            if (resultForCheck.IsNullOrEmpty())
            {
                resultForCheck = ESocketAnswer.Yes.ToString();
            }

            var isEqual = result.Equals( resultForCheck, StringComparison.InvariantCultureIgnoreCase );

            if (isEqual == false )
            {
                throw new VerificationException();
            }
            Log.Info($"SendInfoToClientAndValidateResult Ended {result}");
        }


        /// <summary>
        /// Gets the research entity.
        /// </summary>
        /// <returns></returns>
        private async Task<Research> GetResearchEntity()
        {
            var res = new Research();
            if (ApplicationType == EAppType.Server)
            {
                res.CountOfPerceptonNodes = NetworkConfiguration.N;
                res.CountOfPerceptrons = NetworkConfiguration.K;
                res.WeightLimit = NetworkConfiguration.L;
                res.TypeOfValues = NetworkConfiguration.ValueType.Value;
                res.Date = DateTime.UtcNow;
                res = await dbHelper.AddOrUpdateItem(res);

                await SendInfoToClientAndValidateResult( res.Id.ToString());

            }
            else
            {
                res.Id = int.Parse(await SocketWorker.GetMessageAsync());
                await SendYes();

                res.CountOfPerceptonNodes = NetworkConfiguration.N;
                res.CountOfPerceptrons = NetworkConfiguration.K;
                res.WeightLimit = NetworkConfiguration.L;
                res.Date = DateTime.UtcNow;
                res.TypeOfValues = NetworkConfiguration.ValueType.Value;
                res = await dbHelper.AddOrUpdateItem(res);
            }
            return res;
        }

        /// <summary>
        ///     Sockets the worker on error.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.IO.ErrorEventArgs" /> instance containing the event data.</param>
        private void SocketWorkerOnError(object sender, ErrorEventArgs args)
        {
            OnError(args.ErrorMessage);
        }

        private void SocketWorkerOnCreated()
        {
            OnSocketCreated();
        }

        /// <summary>
        ///     Sockets the worker on connected.
        /// </summary>
        private void SocketWorkerOnConnected()
        {
            OnConnected();
            var result = RunResearch().Result; 
        }

        /// <summary>
        /// Runs the research.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> RunResearch()
        {
            try
            {
                await SyncParams();

                OnParametersCoordinated();
                await StartResearch();
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message, exception);
                OnError(exception.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Starts the research.
        /// </summary>
        private async Task StartResearch()
        {
            Log.Info("StartResearch Started");

            var pcInfo = await SaveInfoAboutPc();

            try
            {
                for (var i = 0; i < NumberOfTests; i++)
                {
                    Log.Info(" Test Started");
                    //if (await IfResearchStoped()) break;
                    var report = await dbHelper.AddOrUpdateItem(new TestResult
                    {
                        NumberOfTest = i + 1,
                        SyncOperationsCount = 0,
                        ResearchId = ResearchEntity.Id
                    });

                    var tpcm = new TPM(NetworkConfiguration);

                    var stateInfo = new CompareOutputsInfo();

                    var elapsedTime = Stopwatch.StartNew();

                    for (var j = 0; j < LimitOfAttempts; j++)
                    {
                        stateInfo = await GetSendOutputAsync(tpcm, stateInfo.HandlerOutput, stateInfo.Inputs);

                        var testReport = new TestReport {NumberOfTest = j};

                        if (stateInfo.IsOutputsEquals)
                        {
                            testReport.IsOutputMach = true;

                            await CompareWeights(tpcm, testReport);

                            if (testReport.IsWeightMach || await IfSynk(testReport))
                            {
                                testReport.IsWeightMach = true;
                                await CreateAttempEntity(testReport, report, stateInfo.MainOutput, stateInfo.Inputs);
                                break;
                            }

                            var calcTime = Stopwatch.StartNew();
                            tpcm.Synchronize();
                            calcTime.Stop();
                            testReport.StopwatchCalculateWeight = calcTime;
                            report.SyncOperationsCount++;
                        }
                        else
                        {
                            testReport.IsOutputMach = false;
                        }

                        await CreateAttempEntity(testReport, report, stateInfo.MainOutput, stateInfo.Inputs);
                    }

                    elapsedTime.Stop();
                    report.IsSyncSucceed = dbHelper.ExecuteQuery(
                        (context, r) =>
                            context.TestResults.First(x => x.Id == r.Id).AttemptResult.Any(x => x.IsWeightMach), report);

                    report.ResultWeights = tpcm.GetWeightsStrings();

                    report = await dbHelper.AddOrUpdateItem(report);
                    OnTestComplete(i);
                    Log.Info(" Test Ended");
                }
                OnResearchComplete();
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message, exception);
                OnError(exception.Message);
            }
            finally
            {
                Log.Info("StartResearch Ended");
            }
        }

        private async Task<PCInfo> SaveInfoAboutPc()
        {

            Log.Info("SaveInfoAboutPc Starte0d");
            var pcInfoEntry = PCInfo.GetCurrentPCInfo();
            pcInfoEntry.AppType = ApplicationType;
            pcInfoEntry.ResearchId = ResearchEntity.Id;

            var res = await dbHelper.AddOrUpdateItem(pcInfoEntry);
            Log.Info("SaveInfoAboutPc Ended");
            return res;
        }

        private async Task CompareWeights(ITPM tpcm, TestReport testReport)
        {
            Log.Info(" CompareWeights Started");
            var str = string.Empty;

            var weights = tpcm.GetTPMWeights();

            str = weights.SelectMany(weight => weight).Aggregate(str,
                (current, complex) =>
                    current + complex.ToString());

            SHA512 sha512 = new SHA512Managed();
            Log.Info("ComputeHash Started");
            var hash = sha512.ComputeHash(Encoding.Unicode.GetBytes(str));

            var sendedHash = Encoding.Unicode.GetString(hash);

            Log.Info("ComputeHash Ended");

            string receiveHash;
            var receiveTime = new Stopwatch();
            var sendTime = new Stopwatch();

            if (ApplicationType == EAppType.Server)
            {
                sendTime.Start();
                await SendInfoToClientAndValidateResult(sendedHash);
                sendTime.Stop();
                receiveTime.Start();
                receiveHash = await SocketWorker.GetMessageAsync();
                await SendYes();
                receiveTime.Stop();
            }

            else
            {
                receiveTime.Start();
                receiveHash = await SocketWorker.GetMessageAsync();
                await SendYes();
                receiveTime.Stop();
                sendTime.Start();
                await SendInfoToClientAndValidateResult(sendedHash);
                sendTime.Stop();
            }

            testReport.StopwatchSendMessage = sendTime;
            testReport.StopwatchReciveMessage = receiveTime;
            testReport.IsWeightMach = sendedHash.Equals(receiveHash);
            Log.Info(" CompareWeights Ended");
        }

        /// <summary>
        ///     Gets the send output.
        /// </summary>
        /// <param name="tpm">The TPCM.</param>
        /// <param name="handlerOutput">The handler output.</param>
        /// <param name="oldInputs"></param>
        /// <returns></returns>
        private async Task<CompareOutputsInfo> GetSendOutputAsync(ITPM tpm, string handlerOutput, IEnumerable<IEnumerable<string>> oldInputs)
        {
            Log.Info(" GetSendOutputAsync Started");
            var result = new CompareOutputsInfo(); 

            var isInputsGenerationNeed = handlerOutput.IsNullOrEmpty() || handlerOutput != tpm.Output;


            result.Inputs = isInputsGenerationNeed ? await GetInputs(tpm) : oldInputs;

            result.MainOutput = tpm.GetOutput(result.Inputs);
            if (ApplicationType == EAppType.Server)
            {
                await SendInfoToClientAndValidateResult(result.MainOutput);
                result.HandlerOutput = await SocketWorker.GetMessageAsync();
                await SendYes();
            }
            else
            {
                result.HandlerOutput = await SocketWorker.GetMessageAsync();
                await SendYes();
                await SendInfoToClientAndValidateResult(result.MainOutput);
            }
            Log.Info(" GetSendOutputAsync Ended");
            return result;
        }

        /// <summary>
        /// Gets the inputs.
        /// </summary>
        /// <param name="tpm">The TPCM.</param>
        /// <returns></returns>
        private async Task<IEnumerable<IEnumerable<string>>> GetInputs(ITPM tpm)
        {
            Log.Info(" GetInputs Started");
            IEnumerable<IEnumerable<string>> inputs;
            if( ApplicationType == EAppType.Server )
            {
                inputs = tpm.GenerateInput( NetworkConfiguration );
                await SendInfoToClientAndValidateResult( inputs.InputsToString() );
            }
            else
            {
                var str = await SocketWorker.GetMessageAsync();
                await SendYes();
                inputs = str.GetInputsFromString();
            }
            Log.Info(" GetInputs Ended");
            return inputs;
        }

        /// <summary>
        ///     Ifs the synk.
        /// </summary>
        /// <param name="testReport">The test report.</param>
        /// <returns></returns>
        private async Task<bool> IfSynk(TestReport testReport)
        {
            Log.Info(" IfSynk Started");
            var syncText = testReport.IsWeightMach
                ? ESocketAnswer.Yes.ToString()
                : ESocketAnswer.No.ToString();
            var answer = String.Empty;

            if (ApplicationType == EAppType.Server)
            {
                await SendInfoToClientAndValidateResult(syncText);
                answer = await SocketWorker.GetMessageAsync();
                await SendYes();
            }
            else
            {
                answer = await SocketWorker.GetMessageAsync();
                await SendYes();
                await SendInfoToClientAndValidateResult( syncText );
            }
            Log.Info(" IfSynk Ended");
            return answer.Equals(ESocketAnswer.Yes.ToString());
        }

        /// <summary>
        /// Creates the attemp entity.
        /// </summary>
        /// <param name="report">The report.</param>
        /// <param name="testResultEntity">The test result entity.</param>
        /// <param name="outputs">The outputs.</param>
        /// <param name="inputs">The inputs.</param>
        /// <returns></returns>
        private async Task CreateAttempEntity(TestReport report, TestResult testResultEntity, string outputs, IEnumerable<IEnumerable<string>> inputs  )
        {
            await  dbHelper.AddOrUpdateItem(new AttemptResult
            {
                NumberOfTest = report.NumberOfTest,
                ElipsatedTime = decimal.Round(report.ElipsatedTime, 10),
                IsOutputMach = report.IsOutputMach,
                IsWeightMach = report.IsWeightMach,
                TestResultId = testResultEntity.Id,
                TimeToCalculateWeight = decimal.Round(report.TimeToCalculateWeight,10),
                TimeToReciveMessage = decimal.Round(report.TimeToReciveMessage, 10),
                TimeToSendMessage = decimal.Round(report.TimeToSendMessage, 10), 
                Inputs = inputs.InputsToString(),
                Output = outputs
            });
            
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            _stop = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (_isDisposed)
            {
                return;
            }

            SocketWorker.Connected -= SocketWorkerOnConnected;
            SocketWorker.Error -= SocketWorkerOnError;
            SocketWorker.Dispose();
            Reports.Clear();

            _isDisposed = true;
        }

        #endregion

        #region Events

        public event SocketCreatedHandler SocketCreated;


        private void OnSocketCreated()
        {
            SocketCreated?.Invoke(ApplicationType);
        }

        /// <summary>
        /// Occurs when [connected].
        /// </summary>
        public event ConnectedHandler Connected;

        /// <summary>
        /// Called when [connected].
        /// </summary>
        private void OnConnected()
        {
            Connected?.Invoke();
        }

        /// <summary>
        /// Occurs when [error].
        /// </summary>
        public event ErrorHandler Error;

        /// <summary>
        /// Called when [error].
        /// </summary>
        /// <param name="message">The message.</param>
        private void OnError(string message)
        {
            SocketWorker.Close();
            Error?.Invoke(this, new ErrorEventArgs(message));
        }

        /// <summary>
        /// Occurs when [test complete].
        /// </summary>
        public event TestCompleteHandler TestComplete;

        /// <summary>
        /// Called when [test complete].
        /// </summary>
        /// <param name="testNumber">The test number.</param>
        private void OnTestComplete(int testNumber)
        {
            TestComplete(this, new TestCompleteArgs(NumberOfTests, testNumber));
        }

        /// <summary>
        /// Occurs when [research complete].
        /// </summary>
        public event ResearchCompleteHandler ResearchComplete;

        /// <summary>
        /// Called when [research complete].
        /// </summary>
        private void OnResearchComplete()
        {
            ResearchComplete?.Invoke();
        }

        /// <summary>
        /// Occurs when [parameters coordinated].
        /// </summary>
        public event ParametersCoordinatedHandler ParametersCoordinated;

        /// <summary>
        /// Called when [parameters coordinated].
        /// </summary>
        public void OnParametersCoordinated()
        {
            var handler = ParametersCoordinated;
            handler?.Invoke(NetworkConfiguration, NumberOfTests);
        }

        #endregion

        #region Delegates

        public delegate void SocketCreatedHandler(EAppType appType);

        public delegate void ConnectedHandler();

        public delegate void ErrorHandler(object seneder, ErrorEventArgs args);

        public delegate void ParametersCoordinatedHandler(NetworkConfig config, int countOfTests);

        public delegate void ResearchCompleteHandler();

        public delegate void TestCompleteHandler(object sender, TestCompleteArgs args);

        #endregion

    }
}