﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NeuralNetworks.NeuralNetwork;
using NeuralNetworksInfrastructure.Enum;
using NeuralNetworksInfrastructure.EventArgs;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;
using NeuralNetworksInfrastructure.Reports;
using NeuralNetworksResults.Context;
using NeuralNetworksResults.Models;
using NeuralNetworksResults.NeuralNetworksResult;

namespace NeuralNetworks.NetworkReserch
{
    public sealed class LocalReserch : IResearch
    {
        #region Fields

        private readonly DataBaseHelper<NeuralNetworksResultDBContext> dbHelper;
        private Random _random;
        private readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the number of tests.
        /// </summary>
        /// <value>
        ///     The number of tests.
        /// </value>
        public int NumberOfTests { get; }

        /// <summary>
        ///     Gets or sets the limit of attempts.
        /// </summary>
        /// <value>
        ///     The limit of attempts.
        /// </value>
        public int LimitOfAttempts { get; }

        /// <summary>
        ///     Gets or sets the network configuration.
        /// </summary>
        /// <value>
        ///     The network configuration.
        /// </value>
        public NetworkConfig NetworkConfiguration { get; }

        /// <summary>
        ///     Gets or sets the reports.
        /// </summary>
        /// <value>
        ///     The reports.
        /// </value>
        public List<Report> Reports { get; }

        /// <summary>
        ///     Gets or sets the research.
        /// </summary>
        /// <value>
        ///     The research.
        /// </value>
        public Research ResearchEntity { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is attempts must be logged in database.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is attempts must be logged in database; otherwise, <c>false</c>.
        /// </value>
        public bool IsAttemptsMustBeLoggedInDb{ get;}

        #endregion

        #region  Contructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="Research" /> class from being created.
        /// </summary>
        private LocalReserch()
        {
            dbHelper = new DataBaseHelper<NeuralNetworksResultDBContext>();
            Reports = new List<Report>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Research" /> class.
        /// </summary>
        /// <param name="limitOfAttempts">The limit of attempts.</param>
        /// <param name="numberOfTest">The number of test.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="isAttemptsMustBeLoggedInDb"></param>
        public LocalReserch(int limitOfAttempts, int numberOfTest,
            NetworkConfig configuration, bool isAttemptsMustBeLoggedInDb) :this()
        {
            LimitOfAttempts = limitOfAttempts;
            NumberOfTests = numberOfTest;
            NetworkConfiguration = configuration;
            IsAttemptsMustBeLoggedInDb = isAttemptsMustBeLoggedInDb;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the research entity.
        /// </summary>
        /// <returns></returns>
        private async Task<Research> GetResearchEntity()
        {
            var res = await dbHelper.AddOrUpdateItem(new Research
            {
                CountOfPerceptonNodes = NetworkConfiguration.N,
                CountOfPerceptrons = NetworkConfiguration.K,
                WeightLimit = NetworkConfiguration.L,
                TypeOfValues = NetworkConfiguration.ValueType.Value,
                Date = DateTime.UtcNow
            });

            return res;
        }

        
        /// <summary>
        ///     Runs the research.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> RunResearch()
        {
            try
            {
                await StartResearch();
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message, exception);
                OnError(exception.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Starts the research.
        /// </summary>
        private async Task StartResearch()
        {
            Log.Info("StartResearch Started");
            ResearchEntity =await GetResearchEntity();
            var pcInfo = await SaveInfoAboutPc();

            try
            {
                for (var i = 0; i < NumberOfTests; i++)
                {
                    var report = await dbHelper.AddOrUpdateItem(new TestResult
                    {
                        NumberOfTest = i + 1,
                        SyncOperationsCount = 0,
                        ResearchId = ResearchEntity.Id
                    });

                    var outs = new List<string>();
                    IEnumerable<IEnumerable<string>> inputs = new IEnumerable<string>[0];
                    var tpmA = TPM.GetTPM(NetworkConfiguration);
                    var tpmB = TPM.GetTPM(NetworkConfiguration);
                    var tpmLists = new List<TPM> { tpmA, tpmB };

                    TestReport testReport;
                    var j = 0;
                    do
                    {
                        testReport = new TestReport { NumberOfTest = j };


                        if (outs.Any() == false || outs[0] != outs[1])
                        {
                            //2. Generate random input vector X
                            inputs = tpmLists.First().GenerateInput(tpmLists.First().GetNetworkConfiguration());
                        }

                        //4. Compute the value of the output neuron
                        outs = tpmLists.AsParallel().Select(x => x.GetOutput(inputs)).ToList();

                        testReport.IsOutputMach = outs.First() == outs.Last();


                        var weights = tpmLists.AsParallel().Select(x => x.GetTPMWeights()).ToList();
                        //4. Compare the values of both tree parity machines
                        testReport.IsWeightMach = TPM.CompareWeights(NetworkConfiguration, weights.First(), weights.Last());

                        if (testReport.IsOutputMach)
                        {
                            if (testReport.IsWeightMach)
                            {
                                await CreateAttempEntities(testReport, report, tpmA, inputs, tpmB);
                                break;
                            }
                            var calcTime = Stopwatch.StartNew();
                            //Outputs are same: one of the suitable learning rules is applied to the weights
                            tpmLists.AsParallel().ForAll(x => x.Synchronize());
                            testReport.StopwatchCalculateWeight = calcTime;
                            report.SyncOperationsCount++;
                        }
                        await CreateAttempEntities(testReport, report, tpmA, inputs, tpmB);

                        j++;
                    } while (j < LimitOfAttempts);
                    report.AttemptsCount = j;
                    report.IsSyncSucceed = testReport.IsWeightMach;

                    report.ResultWeights = tpmA.GetWeightsStrings();

                    report = await dbHelper.AddOrUpdateItem(report);
                    OnTestComplete(i);
                    Log.Info(" Test Ended");

                }
                OnResearchComplete();
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message, exception);
                OnError(exception.Message);
            }
            finally
            {
                Log.Info("StartResearch Ended");
            }
        }

        private async Task CreateAttempEntities(TestReport testReport, TestResult report, TPM tpmA, IEnumerable<IEnumerable<string>> inputs, TPM tpmB)
        {
            await CreateAttempEntity(testReport, report, tpmA.Output, inputs);
            await CreateAttempEntity(testReport, report, tpmB.Output, inputs);
        }

        private async Task<PCInfo> SaveInfoAboutPc()
        {
            Log.Info("SaveInfoAboutPc Started");
            var pcInfoEntry = PCInfo.GetCurrentPCInfo();
            pcInfoEntry.AppType = EAppType.Undefined;
            pcInfoEntry.ResearchId = ResearchEntity.Id;

            var res = await dbHelper.AddOrUpdateItem(pcInfoEntry);
            Log.Info("SaveInfoAboutPc Ended");
            return res;
        }

      

        /// <summary>
        ///     Creates the attemp entity.
        /// </summary>
        /// <param name="report">The report.</param>
        /// <param name="testResultEntity">The test result entity.</param>
        /// <param name="outputs">The outputs.</param>
        /// <param name="inputs">The inputs.</param>
        /// <returns></returns>
        private async Task CreateAttempEntity(TestReport report, TestResult testResultEntity, string outputs,
            IEnumerable<IEnumerable<string>> inputs)
        {
            if (IsAttemptsMustBeLoggedInDb == false) return;
            await dbHelper.AddOrUpdateItem(new AttemptResult
            {
                NumberOfTest = report.NumberOfTest,
                ElipsatedTime = decimal.Round(report.ElipsatedTime, 10),
                IsOutputMach = report.IsOutputMach,
                IsWeightMach = report.IsWeightMach,
                TestResultId = testResultEntity.Id,
                TimeToCalculateWeight = decimal.Round(report.TimeToCalculateWeight, 10),
                TimeToReciveMessage = decimal.Round(report.TimeToReciveMessage, 10),
                TimeToSendMessage = decimal.Round(report.TimeToSendMessage, 10),
                Inputs = inputs.InputsToString(),
                Output = outputs
            });
        }


      

        #endregion

        #region Events
        
        /// <summary>
        ///     Occurs when [error].
        /// </summary>
        public event ErrorHandler Error;

        /// <summary>
        ///     Called when [error].
        /// </summary>
        /// <param name="message">The message.</param>
        private void OnError(string message)
        {
            Error?.Invoke(this, new ErrorEventArgs(message));
        }

        /// <summary>
        ///     Occurs when [test complete].
        /// </summary>
        public event TestCompleteHandler TestComplete;

        /// <summary>
        ///     Called when [test complete].
        /// </summary>
        /// <param name="testNumber">The test number.</param>
        private void OnTestComplete(int testNumber)
        {
            TestComplete(this, new TestCompleteArgs(NumberOfTests, testNumber));
        }

        /// <summary>
        ///     Occurs when [research complete].
        /// </summary>
        public event ResearchCompleteHandler ResearchComplete;

        /// <summary>
        ///     Called when [research complete].
        /// </summary>
        private void OnResearchComplete()
        {
            ResearchComplete?.Invoke();
        }

        #endregion

        #region Delegates


        public delegate void ErrorHandler(object seneder, ErrorEventArgs args);

        public delegate void ResearchCompleteHandler();

        public delegate void TestCompleteHandler(object sender, TestCompleteArgs args);

        #endregion
    }
}