using System;
using NeuralNetworksInfrastructure.Algebra;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworks.NeuralNetwork
{
    public sealed class ComplexPerceptron : PerceptronBase<Complex>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexPerceptron" /> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public ComplexPerceptron(NetworkConfig config) : base( config, 2 )
        {
            CreateWeights();
        }

        /// <summary>
        ///     Creates the perceptron.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        public override IPerceptron CreatePerceptron(NetworkConfig config)
        {
            return new ComplexPerceptron( config );
        }

        /// <summary>
        ///     Gets the empty value.
        /// </summary>
        /// <returns></returns>
        protected override string GetEmptyValue()
        {
            return new Complex( 0, 0 ).ToString();
        }

        /// <summary>
        ///     Sigmas the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <returns></returns>
        public override string Sigma(string output)
        {
            var complex = new Complex();
            var parced = ParseWithCast( output );
            var deg = parced.Deq();

            if ( ( ( deg <= 45 ) && ( deg > 0 ) ) || ( ( deg > 315 ) && ( deg <= 360 ) ) )
                complex = new Complex( 1, 0 );
            else if( ( deg > 45 ) && ( deg <= 135 ) )
                complex = new Complex( 0, 1 );
            else if( ( deg > 135 ) && ( deg <= 225 ) )
                complex = new Complex( -1, 0 );
            else if( ( deg > 225 ) && ( deg <= 315 ) )
                complex = new Complex( 0, -1 );
            return complex.ToString();
        }


        /// <summary>
        ///     Normalizes the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected override Complex NormalizeValue(Complex value)
        {
            if( Math.Abs( value.Real ) > PerceptronConfiguration.L )
                value = new Complex( Math.Sign( value.Real )*PerceptronConfiguration.L, value.I );
            if( Math.Abs( value.I ) > PerceptronConfiguration.L )
                value = new Complex( value.Real, Math.Sign( value.I )*PerceptronConfiguration.L );


            return value;
        }

        /// <summary>
        ///     Parse the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected override object Parse(string value)
        {
            return Complex.Parse( value );
        }

        /// <summary>
        ///     Gets the random value.
        /// </summary>
        /// <returns></returns>
        public override string GetRandomValue()
        {
            return new Complex( NextRandomInRange(), NextRandomInRange() ).ToString();
        }

    }
}