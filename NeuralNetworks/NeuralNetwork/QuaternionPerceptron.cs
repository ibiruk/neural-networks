﻿using System;
using System.Linq;
using NeuralNetworksInfrastructure.Algebra;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworks.NeuralNetwork
{
    /// <summary>
    /// </summary>
    /// <seealso cref="NeuralNetworks.NeuralNetwork.PerceptronBase{NeuralNetworksInfrastructure.Algebra.Quaternion}" />
    public class QuaternionPerceptron : PerceptronBase<Quaternion>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="QuaternionPerceptron"/> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public QuaternionPerceptron(NetworkConfig config) : base(config, 4)
        {
            CreateWeights();
        }

        /// <summary>
        ///     Creates the perceptron.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        public override IPerceptron CreatePerceptron(NetworkConfig config)
        {
            return new QuaternionPerceptron(config);
        }

        /// <summary>
        ///     Gets the empty value.
        /// </summary>
        /// <returns></returns>
        protected override string GetEmptyValue()
        {
            return new Quaternion(0, 0, 0, 0).ToString();
        }

        /// <summary>
        ///     Normalizes the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected override Quaternion NormalizeValue(Quaternion value)
        {
            if (Math.Abs(value.Real) > PerceptronConfiguration.L)
                value = new Quaternion(Math.Sign(value.Real) *PerceptronConfiguration.L, value.I, value.J, value.K);
            if (Math.Abs(value.I) > PerceptronConfiguration.L)
                value = new Quaternion(value.Real, Math.Sign(value.I)*PerceptronConfiguration.L, value.J, value.K);
            if (Math.Abs(value.J) > PerceptronConfiguration.L)
                value = new Quaternion(value.Real, value.I, Math.Sign(value.J)*PerceptronConfiguration.L, value.K);
            if (Math.Abs(value.K) > PerceptronConfiguration.L)
                value = new Quaternion(value.Real, value.I, value.J, Math.Sign(value.K)*PerceptronConfiguration.L);
            return value;
        }

        /// <summary>
        ///     Sigmas the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <returns></returns>
        public override string Sigma(string output)
        {
            var sig = ParseWithCast( output );
            var maxComponent = sig.GetComponents().Max();

            Quaternion result;

            if (Math.Min(maxComponent, sig.Real) >= 0)
            {
                result = new Quaternion(1, 0, 0, 0);
            }
            else if(Math.Min(maxComponent, sig.Real) < 0)
            {
                result = new Quaternion(-1, 0, 0, 0);
            }
            else if (Math.Min(maxComponent, sig.I) >= 0)
            {
                result = new Quaternion(0, 1, 0, 0);
            }
            else if (Math.Min(maxComponent, sig.I) <0)
            {
                result = new Quaternion(0, -1, 0, 0);
            }
            else if (Math.Min(maxComponent, sig.J) >=0)
            {
                result = new Quaternion(0, 0, 1, 0);
            }
            else if (Math.Min(maxComponent, sig.J) < 0)
            {
                result = new Quaternion(0, 0, -1, 0);
            }
            else if (Math.Min(maxComponent, sig.K) >= 0)
            {
                result = new Quaternion(0, 0, 0, 1);
            }
            else 
            {
                result = new Quaternion(0, 0, 0, -1);
            }
            return result.ToString();
        }

        /// <summary>
        ///     Parces the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected override object Parse(string value)
        {
            return Quaternion.Parse( value );
        }

        /// <summary>
        ///     Gets the random value.
        /// </summary>
        /// <returns></returns>
        public override string GetRandomValue()
        {
            return
                new Quaternion(NextRandomInRange(), NextRandomInRange(), NextRandomInRange(), NextRandomInRange())
                    .ToString();
        }

    }
}