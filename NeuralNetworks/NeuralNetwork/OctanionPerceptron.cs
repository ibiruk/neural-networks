﻿using System;
using System.Linq;
using NeuralNetworksInfrastructure.Algebra;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworks.NeuralNetwork
{
    /// <summary>
    /// </summary>
    /// <seealso cref="Octonion" />
    public class OctonionPerceptron : PerceptronBase<Octonion>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QuaternionPerceptron"/> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public OctonionPerceptron(NetworkConfig config) : base( config, 8 )
        {
            CreateWeights();
        }

        /// <summary>
        ///     Creates the perceptron.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        public override IPerceptron CreatePerceptron(NetworkConfig config)
        {
            return new OctonionPerceptron( config );
        }

        /// <summary>
        ///     Gets the empty value.
        /// </summary>
        /// <returns></returns>
        protected override string GetEmptyValue()
        {
            return new Octonion( 0, 0, 0, 0, 0, 0, 0, 0 ).ToString();
        }

        /// <summary>
        ///     Normalizes the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected override Octonion NormalizeValue(Octonion value)
        {
            if( Math.Abs( value.Real ) > PerceptronConfiguration.L )
                value = new Octonion( Math.Sign( value.Real )*PerceptronConfiguration.L, value.I, value.J, value.K, value.L, value.IL, value.JL, value.KL );

            if( Math.Abs( value.I ) > PerceptronConfiguration.L )
                value = new Octonion( value.Real, Math.Sign( value.I )*PerceptronConfiguration.L, value.J, value.K, value.L, value.IL, value.JL, value.KL );

            if( Math.Abs( value.J ) > PerceptronConfiguration.L )
                value = new Octonion( value.Real, value.I, Math.Sign( value.J )*PerceptronConfiguration.L, value.K, value.L, value.IL, value.JL, value.KL );

            if( Math.Abs( value.K ) > PerceptronConfiguration.L )
                value = new Octonion( value.Real, value.I, value.J, Math.Sign( value.K )*PerceptronConfiguration.L, value.L, value.IL, value.JL, value.KL );

            if( Math.Abs( value.L ) > PerceptronConfiguration.L )
                value = new Octonion( value.Real, value.I, value.J, value.K, Math.Sign( value.L )*PerceptronConfiguration.L, value.IL, value.JL, value.KL );

            if( Math.Abs( value.IL ) > PerceptronConfiguration.L )
                value = new Octonion( value.Real, value.I, value.J, value.K, value.L, Math.Sign( value.IL )*PerceptronConfiguration.L, value.JL, value.KL );

            if( Math.Abs( value.JL ) > PerceptronConfiguration.L )
                value = new Octonion( value.Real, value.I, value.J, value.K, value.L, value.IL, Math.Sign( value.JL )*PerceptronConfiguration.L, value.KL );

            if( Math.Abs( value.KL ) > PerceptronConfiguration.L )
                value = new Octonion( value.Real, value.I, value.J, value.K, value.L, value.IL, value.JL, Math.Sign( value.KL )*PerceptronConfiguration.L );

            return value;
        }

        /// <summary>
        ///     Sigmas the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <returns></returns>
        public override string Sigma(string output)
        {
            var sig = ParseWithCast( output );
            var maxComponent = sig.GetComponents().Max();

            Octonion result;

            if( Math.Min( maxComponent, sig.Real ) >= 0 )
            {
                result = new Octonion( 1, 0, 0, 0, 0, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.Real ) < 0 )
            {
                result = new Octonion( -1, 0, 0, 0, 0, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.I ) >= 0 )
            {
                result = new Octonion( 0, 1, 0, 0, 0, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.I ) < 0 )
            {
                result = new Octonion( 0, -1, 0, 0, 0, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.J ) >= 0 )
            {
                result = new Octonion( 0, 0, 1, 0, 0, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.J ) < 0 )
            {
                result = new Octonion( 0, 0, -1, 0, 0, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.K ) >= 0 )
            {
                result = new Octonion( 0, 0, 0, 1, 0, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.K ) < 0 )
            {
                result = new Octonion( 0, 0, 0, -1, 0, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.L ) >= 0 )
            {
                result = new Octonion( 0, 0, 0, 0, 1, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.L ) < 0 )
            {
                result = new Octonion( 0, 0, 0, 0, -1, 0, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.IL ) >= 0 )
            {
                result = new Octonion( 0, 0, 0, 0, 0, 1, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.IL ) < 0 )
            {
                result = new Octonion( 0, 0, 0, 0, 0, -1, 0, 0 );
            }
            else if( Math.Min( maxComponent, sig.JL ) >= 0 )
            {
                result = new Octonion( 0, 0, 0, 0, 0, 0, 1, 0 );
            }
            else if( Math.Min( maxComponent, sig.JL ) < 0 )
            {
                result = new Octonion( 0, 0, 0, 0, 0, 0, -1, 0 );
            }
            else if( Math.Min( maxComponent, sig.KL ) >= 0 )
            {
                result = new Octonion( 0, 0, 0, 0, 0, 0, 0, 1 );
            }
            else
            {
                result = new Octonion( 0, 0, 0, 0, 0, 0, 0, -1 );
            }

            return result.ToString();
        }

        /// <summary>
        ///     Parces the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected override object Parse(string value)
        {
            return Octonion.Parse( value );
        }

        /// <summary>
        ///     Gets the random value.
        /// </summary>
        /// <returns></returns>
        public override string GetRandomValue()
        {
            return
                new Octonion( NextRandomInRange(), NextRandomInRange(), NextRandomInRange(), NextRandomInRange(), NextRandomInRange(), NextRandomInRange(),
                        NextRandomInRange(), NextRandomInRange() )
                    .ToString();
        }
    }
}