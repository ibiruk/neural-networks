﻿using System;
using NeuralNetworksInfrastructure.Algebra;
using NeuralNetworksInfrastructure.Enum;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworks.NeuralNetwork
{
    /// <summary>
    /// </summary>
    /// <seealso cref="Simple" />
    public class SimpleArithmeticPerceptron : PerceptronBase<Simple>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SimpleArithmeticPerceptron" /> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public SimpleArithmeticPerceptron(NetworkConfig config) : base(config, 1 )
        {
            CreateWeights();
        }

        /// <summary>
        ///     Creates the perceptron.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        public override IPerceptron CreatePerceptron(NetworkConfig config)
        {
            return new SimpleArithmeticPerceptron(config);
        }

        /// <summary>
        ///     Gets the empty value.
        /// </summary>
        /// <returns></returns>
        protected override string GetEmptyValue()
        {
            return 0.ToString();
        }


        /// <summary>
        ///     Sigmas the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <returns></returns>
        public override string Sigma(string output)
        {
            return ParseWithCast(output).Real > (double)decimal.Zero ? decimal.One.ToString() : decimal.MinusOne.ToString();
        }

        /// <summary>
        ///     Normalizes the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected override Simple NormalizeValue(Simple value)
        {
            if (Math.Abs(value.Real) > PerceptronConfiguration.L)
                value.Real = Math.Sign(value.Real)*PerceptronConfiguration.L;
            return value;
        }

        /// <summary>
        ///     Parces the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        protected override object Parse(string value)
        {
            return Simple.Parse(value);
        }

        /// <summary>
        ///     Gets the random value.
        /// </summary>
        /// <returns></returns>
        public override string GetRandomValue()
        {
            return Ranomizer.Next(-PerceptronConfiguration.L, PerceptronConfiguration.L).ToString();
        }
    }
}