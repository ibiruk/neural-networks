﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using NeuralNetworksInfrastructure.Enum;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworks.NeuralNetwork
{
    public class TPM : ITPM
    {
        private readonly NetworkConfig networkConfiguration;
        private readonly List<IPerceptron> Perceptrons;
        public string _output;
        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Output { get { return _output; } }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TPM" /> class.
        /// </summary>
        private TPM()
        {
            Perceptrons = new List<IPerceptron>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TPM" /> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public TPM(NetworkConfig config) : this()
        {
            Log.Info(" TPM constructor Started");
            networkConfiguration = config;

            var emptyInstanse = GetEmptyPerceptron(config);

            for (var i = 0; i < networkConfiguration.K; i++)
            {
                var perceptron = (IPerceptron) emptyInstanse;
                if (perceptron != null)
                    Perceptrons.Add(perceptron.CreatePerceptron(networkConfiguration));
            }
            Log.Info(" TPM constructor Ended");
        }

        /// <summary>
        ///     Gets the output.
        /// </summary>
        /// <param name="aInputs">a inputs.</param>
        /// <returns></returns>
        public string GetOutput(IEnumerable<IEnumerable<string>> aInputs)
        {
            Log.Info(" GetOutput  Started");
            var perceptron = (IPerceptron) GetEmptyPerceptron(networkConfiguration);
            _output = perceptron.GetMinOutput();
            var inputs = aInputs.ToArray();
            Parallel.ForEach(Perceptrons,
                (element, loopstate, elementIndex) =>
                {
                    _output = perceptron.Multiply(_output, element.GetOutput(inputs[(int)elementIndex]));
                });
            _output = Sigma(_output);
            Log.Info("GetOutput Ended");
            return _output;
        }

        /// <summary>
        ///     Synchronizes this instance.
        /// </summary>
        public void Synchronize()
        {
            Log.Info("Synchronize  Started");
            Perceptrons.AsParallel().ForAll(x => x.AktualizeWeights(_output));
            Log.Info("Synchronize Ended");
        }

        /// <summary>
        ///     Gets the TPM weights.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IEnumerable<string>> GetTPMWeights()
        {
            Log.Info("GetTPMWeights  Started");
            var listTPM = new List<List<string>>();
            for (var i = 0; i < networkConfiguration.K; i++)
            {
                var weights = new List<string>();
                for (var j = 0; j < networkConfiguration.N; j++)
                {
                    weights.Add(Perceptrons[i].GetWeight(j));
                }
                listTPM.Add(weights);
            }
            Log.Info("GetTPMWeights  Ended");
            return listTPM;

        }

        /// <summary>
        /// Gets the weights strings.
        /// </summary>
        /// <returns></returns>
        public string GetWeightsStrings()
        {
            Log.Info("GetWeightsStrings  Started");
            var res = string.Join( Environment.NewLine,
                GetTPMWeights().Select( x => string.Join( " ", x.ToArray() ) ).ToArray() );
            Log.Info("GetWeightsStrings  Ended");
            return res;
        }

        /// <summary>
        ///     Gets the network configuration.
        /// </summary>
        /// <returns></returns>
        public NetworkConfig GetNetworkConfiguration()
        {
            return networkConfiguration;
        }

        /// <summary>
        ///     Generates the input.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        public IEnumerable<IEnumerable<string>> GenerateInput(NetworkConfig config)
        {
            Log.Info("GenerateInput  Started");
            var perceptron = GetEmptyPerceptron(config);
            var list = new List<List<string>>();
            for (var i = 0; i < config.K; i++)
            {
                var innerList = new List<string>();
                for (var j = 0; j < config.N; j++)
                {
                    innerList.Add( ( (IPerceptron)perceptron ).GetRandomInput() );
                }
                list.Add( innerList );
            }
            Log.Info("GenerateInput  Ended");
            return list;
        }

        /// <summary>
        ///     Gets the empty perceptron.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        private object GetEmptyPerceptron(NetworkConfig config)
        {
            switch (config.ValueType)
            {
                case ETypesOfTPM.Int:
                    return new SimpleArithmeticPerceptron(config);
                case ETypesOfTPM.Complex:
                    return new ComplexPerceptron(config);
                case ETypesOfTPM.Quaternion:
                    return new QuaternionPerceptron(config);
                case ETypesOfTPM.Octonion:
                    return new OctonionPerceptron(config);
                default:
                    throw new InvalidEnumArgumentException(config.ValueType.ToString());
            }
        }

        /// <summary>
        ///     Sigmas the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <returns></returns>
        public string Sigma(string output)
        {
            Log.Info("GenerateInput  Started");
            var r =  ((IPerceptron) GetEmptyPerceptron(networkConfiguration)).Sigma(output);
            Log.Info("GenerateInput  Ended");
            return r;
        }

        /// <summary>
        ///     Gets the TPM.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        public static TPM GetTPM(NetworkConfig config)
        {
            return new TPM(config);
        }

        /// <summary>
        ///     Compares the weights.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="secondW">The second w.</param>
        /// <param name="firstW">The first w.</param>
        /// <param name="cc">The cc.</param>
        /// <returns></returns>
        public static bool CompareWeights(NetworkConfig configuration, IEnumerable<IEnumerable<string>> secondW, IEnumerable<IEnumerable<string>> firstW)
        {
            var cc = 0;

            var tpm1 = firstW.Select( x=>x.ToArray() ).ToArray();
            var tpm2 = secondW.Select(x => x.ToArray()).ToArray();

            for (var i = 0; i < tpm1.Length; i++)
            {
                var lA = tpm1[i];
                var lB = tpm2[i];
                for (var j = 0; j < tpm1[0].Length; j++)
                {
                    if (lA[j].Equals(lB[j], StringComparison.CurrentCultureIgnoreCase))
                    {
                        cc++;
                    }
                }
            }
            return cc == configuration.CountOfPerceptrones;
        }
    }
}