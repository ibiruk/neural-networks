﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NeuralNetworksInfrastructure.Algebra;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworks.NeuralNetwork
{
    public abstract class PerceptronBase<T> : IDisposable, IPerceptron where T: INumberSystem<T>, new()
    {
        
        /// <summary>
        ///     Initializes a new instance of the <see cref="PerceptronBase{T}" /> class.
        /// </summary>
        protected PerceptronBase(int componentsCount)
        {
            ValidInputs = GetValidInputs(componentsCount);
            Ranomizer = new CryptoRandom();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PerceptronBase{T}" /> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="componentsCount"></param>
        protected PerceptronBase(NetworkConfig config, int componentsCount) : this(componentsCount)
        {
            PerceptronConfiguration = config;
            Weights = new List<T>();
            Inputs = new List<T>();
        }

        private void GetPermutationWithRecursion(double[] alphabet, double[] arBuffer, int order = 0, List<double[]> permuations = null)
        {
            if( order < arBuffer.Length )
            {
                foreach(var t in alphabet)
                {
                    arBuffer[order] = t;
                    GetPermutationWithRecursion(alphabet, arBuffer, order + 1, permuations);
                }
            }
                
            else if(permuations.Contains( arBuffer ) == false)
            {
                permuations.Add( arBuffer.Clone() as double[] );
            }
        }


        private T[] GetValidInputs(int componentsCount)
        {
            var alphabet = new double[]{1, -1};
            var inputsCount =(int) Math.Pow( 2, componentsCount );
            var bufferList = new List<double[]>();
            var resultArray = new T[inputsCount];


            var componentsArray = new double[componentsCount];



            GetPermutationWithRecursion( alphabet, componentsArray, permuations: bufferList );

            var type = typeof( T );
            var paramsArray = componentsArray.Select( x => typeof( double ) ).ToArray();

            foreach (var item in bufferList)
            {
                var ctor = type.GetConstructor( paramsArray );
                resultArray[bufferList.IndexOf( item )] = ParseWithCast( ctor.Invoke( item.Select( x => x as object ).ToArray() ).ToString() );// o_O
            }
          
            return resultArray;
        }



        protected NetworkConfig PerceptronConfiguration { get; set; }
        protected List<T> Weights { get; set; }
        protected List<T> Inputs { get; set; }
        protected T Output { get; set; }
        protected CryptoRandom Ranomizer { get; set; }
        protected virtual T[] ValidInputs { get; private set; }

        #region IDisposable Members

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Weights.Clear();
            PerceptronConfiguration = null;
            Inputs.Clear();
        }

        #endregion

        #region IPerceptron Members

        /// <summary>
        ///     Gets the output.
        /// </summary>
        /// <param name="aInputs">a inputs.</param>
        /// <returns></returns>
        public string GetOutput(IEnumerable<string> aInputs)
        {
            Inputs = aInputs.Select( ParseWithCast ).ToList();
            Output = ParseWithCast( GetEmptyValue() );
            for(var i = 0; i < PerceptronConfiguration.N; i++)
            {
                var multi = Multiply( Inputs[i], Weights[i] );
                Output = Summ( Output, multi );
            }

            Output = ParseWithCast( Sigma( Output.ToString() ) );
            return Output.ToString();
        }

        /// <summary>
        ///     Gets the random value.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual string GetRandomValue()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetRandomInput()
        {
            return ValidInputs[Ranomizer.Next( ValidInputs.Length )].ToString();
        }

        /// <summary>
        ///     Sigmas the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public virtual string Sigma(string value)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Gets the minimum output.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual string GetMinOutput()
        {
            return new T().GetIdentity().ToString();
        }

        /// <summary>
        ///     Creates the perceptron.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual IPerceptron CreatePerceptron(NetworkConfig config)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Aktualizes the weights.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void AktualizeWeights(string outputA)
        {
            var parced = ParseWithCast( outputA );
            if(Equally( parced, Output ) == false) return;
            for(var i = 0; i < PerceptronConfiguration.N; i++)
            {
                for(var j = 0; j < PerceptronConfiguration.N; j++)
                {
                    Weights[i] = Summ( Weights[i], Multiply( Output, Inputs[j] ) );
                }

                Weights[i] = NormalizeValue( Weights[i] );
            }
            
        }

        /// <summary>
        ///     Gets the weight.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public string GetWeight(int index)
        {
            return Weights[index].ToString();
        }

        /// <summary>
        ///     Multiplies the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="weight">The weight.</param>
        /// <returns></returns>
        public string Multiply(string input, string weight)
        {
            return Multiply( ParseWithCast( input ), ParseWithCast( weight ) ).ToString();
        }

        #endregion

        /// <summary>
        ///     Creates the weights.
        /// </summary>
        protected void CreateWeights()
        {
            for(var i = 0; i < PerceptronConfiguration.N; i++)
            {
                Weights.Add( ParseWithCast( GetRandomValue() ) );
            }
        }

        /// <summary>
        ///     Nexts the random in range.
        /// </summary>
        /// <returns></returns>
        protected int NextRandomInRange(int? range = null)
        {
            if( range == null )
            {
                range = PerceptronConfiguration.L;
            }
            return Ranomizer.Next( -range.Value, range.Value );
        }

        /// <summary>
        ///     Summs the specified first.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns></returns>
        private T Summ(T first, T second)
        {
            var paramA = Expression.Parameter( typeof( T ), "a" );
            var paramB = Expression.Parameter( typeof( T ), "b" );

            var body = Expression.Add( paramA, paramB );

            var add = Expression.Lambda<Func<T, T, T>>( body, paramA, paramB ).Compile();

            return add( first, second );
        }

        /// <summary>
        ///     Multiplies the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="weight">The weight.</param>
        /// <returns></returns>
        private T Multiply(T input, T weight)
        {
            var paramA = Expression.Parameter( typeof( T ), "a" );
            var paramB = Expression.Parameter( typeof( T ), "b" );

            var body = Expression.Multiply( paramA, paramB );

            var multiply = Expression.Lambda<Func<T, T, T>>( body, paramA, paramB ).Compile();

            return multiply( input, weight );
        }

        private bool Equally(T left, T right)
        {
            var paramA = Expression.Parameter(typeof(T), "a");
            var paramB = Expression.Parameter(typeof(T), "b");

            var body = Expression.Equal(paramA, paramB);

            var equally = Expression.Lambda<Func<T, T, bool>>(body, paramA, paramB).Compile();
            return equally( left, right );

        }

        /// <summary>
        ///     Divides the specified first.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns></returns>
        public T Divide(T first, T second)
        {
            var paramA = Expression.Parameter( typeof( T ), "a" );
            var paramB = Expression.Parameter( typeof( T ), "b" );

            var body = Expression.Divide( paramA, paramB );

            var divide = Expression.Lambda<Func<T, T, T>>( body, paramA, paramB ).Compile();

            return divide( first, second );
        }

        /// <summary>
        ///     Parses the with cast.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected T ParseWithCast(string value)
        {
            return (T)Parse( value );
        }

        /// <summary>
        ///     Gets the empty value.
        /// </summary>
        /// <returns></returns>
        protected abstract string GetEmptyValue();

        /// <summary>
        ///     Normalizes the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected abstract T NormalizeValue(T value);

        /// <summary>
        ///     Parces the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected abstract object Parse(string value);
    }
}