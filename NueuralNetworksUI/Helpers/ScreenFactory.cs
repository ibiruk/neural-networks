﻿using NeuralNetworksUI.Bases;

namespace NueuralNetworksUI.Helpers
{
    public static class ScreenFactory<T> where T : ScreenBase, new()
    {
        public static T GetScreen()
        {
            var result = new T();
            result.Initialize();
            return result;
        }
    }
}