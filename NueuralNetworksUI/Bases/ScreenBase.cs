﻿using Caliburn.Micro;

namespace NeuralNetworksUI.Bases
{
    public abstract class ScreenBase : Screen
    {


        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMessage"></param>
        protected abstract void ShowMessage(string strMessage);

        /// <summary>
        /// 
        /// </summary>
        public virtual void Close()
        {
            
        }



        public void Initialize()
        {
            InitializeDefaultValues();
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void InitializeDefaultValues()
        {
            
        }

        // Method invoked on a separate thread that shows the message box.
        // Shows a message box from a separate worker thread.
        public void ShowMessageBoxAsync(string strMessage)
        {
            ShowMessageBoxDelegate caller = ShowMessage;
            caller.BeginInvoke(strMessage, null, null);
        }

        /// <summary>
        /// Tries to close this instance by asking its Parent to initiate shutdown or by asking its corresponding view to close.
        /// Also provides an opportunity to pass a dialog result to it's corresponding view.
        /// </summary>
        /// <param name="dialogResult">The dialog result.</param>
        public override void TryClose(bool? dialogResult = null)
        {
            Close();
            base.TryClose( dialogResult );
        }

        

        private delegate void ShowMessageBoxDelegate(string strMessage);
    }
}