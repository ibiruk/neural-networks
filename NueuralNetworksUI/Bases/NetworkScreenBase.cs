﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NeuralNetworks.NetworkReserch;
using NeuralNetworksInfrastructure.Enum;
using NeuralNetworksInfrastructure.EventArgs;
using NeuralNetworksInfrastructure.Helpers;

namespace NeuralNetworksUI.Bases
{
    public abstract class NetworkScreenBase : ConfigurationScreenBase
    {
        private EAppType _applicationType;
        private string _ipAddress;
        private int _port;

        protected Thread netThread;
        private bool _isProcessStarted;

        /// <summary>
        /// Gets or sets the network research.
        /// </summary>
        /// <value>
        /// The network research.
        /// </value>
        protected NetworkReserch NetworkResearch { get; set; }

        /// <summary>
        /// Gets or sets the type of the application.
        /// </summary>
        /// <value>
        /// The type of the application.
        /// </value>
        public EAppType ApplicationType
        {
            get { return _applicationType; }
            set
            {
                if (value == _applicationType) return;
                _applicationType = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("IsServerSelected");
                NotifyOfPropertyChange("IsCanBeEdited");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is server selected.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is server selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsServerSelected
        {
            get { return ApplicationType == EAppType.Server; }
        }

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        public string IpAddress
        {
            get { return _ipAddress; }
            set
            {
                if (value == _ipAddress) return;
                _ipAddress = value;
                NotifyOfPropertyChange();
            }
        }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public int Port
        {
            get { return _port; }
            set
            {
                if (value == _port) return;
                _port = value;
                NotifyOfPropertyChange();
            }
        }

        /// <summary>
        /// Gets the application types.
        /// </summary>
        /// <value>
        /// The application types.
        /// </value>
        public List<EAppType> ApplicationTypes
        {
            get { return Enum.GetValues(typeof (EAppType)).Cast<EAppType>().ToList(); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is process started.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is process started; otherwise, <c>false</c>.
        /// </value>
        public bool IsProcessStarted
        {
            get { return _isProcessStarted; }
            set
            {
                if (value == _isProcessStarted) return;
                _isProcessStarted = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("IsCanBeEdited");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is can be edited.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is can be edited; otherwise, <c>false</c>.
        /// </value>
        public bool IsCanBeEdited => IsServerSelected && IsProcessStarted == false;
        

        /// <summary>
        /// 
        /// </summary>
        public void GetLocalIPAddress()
        {
            Port = new Random().Next(49152, 65535);
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    IpAddress = ip.ToString();
                    return;
                }
            }
            ShowMessageBoxAsync("Local IP Address Not Found!");
        }

        /// <summary>
        /// Connects this instance.
        /// </summary>
        public void Connect()
        {
            NetworkResearch = ApplicationType == EAppType.Server
                ? new NetworkReserch( IpAddress, Port, 1000000, CountOfTests, Configuration, ApplicationType)
                : new NetworkReserch(IpAddress, Port, ApplicationType);
            NetworkResearch.SocketCreated += NetworkResearchOnSocketCreated;
            NetworkResearch.Connected += OnConnected;
            NetworkResearch.Error += OnError;
            NetworkResearch.ParametersCoordinated += NetworkResearchOnParametersCoordinated;
            NetworkResearch.TestComplete += NetworkResearchOnTestComplete;
            NetworkResearch.ResearchComplete += NetworkResearchOnResearchComplete;

            ThreadPool.QueueUserWorkItem( state => NetworkResearch.CreateSocket() );
            
            //netThread = new Thread( NetworkResearch.CreateSocket ){IsBackground = false};
            
            //netThread.Start();
        }

        /// <summary>
        /// Networks the research on socket created.
        /// </summary>
        /// <param name="appType">Type of the application.</param>
        private void NetworkResearchOnSocketCreated(EAppType appType)
        {
            IsProcessStarted = true;
        }

        /// <summary>
        /// Networks the research on research complete.
        /// </summary>
        private void NetworkResearchOnResearchComplete()
        {
            ShowMessageBoxAsync("Завершено");
            Close();
        }

        /// <summary>
        /// Networks the research on test complete.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The arguments.</param>
        private void NetworkResearchOnTestComplete(object sender, TestCompleteArgs args)
        {
            DoneTestsCount++;
        }

        /// <summary>
        /// Networks the research on parameters coordinated.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="countOfTests">The count of tests.</param>
        private void NetworkResearchOnParametersCoordinated(NetworkConfig config, int countOfTests)
        {
            ShowMessageBoxAsync("Параметры установлены");

            Configuration = config;
            CountOfTests = countOfTests;
            DoneTestsCount = 0;
        }

        /// <summary>
        /// Called when [error].
        /// </summary>
        /// <param name="seneder">The seneder.</param>
        /// <param name="args">The <see cref="ErrorEventArgs"/> instance containing the event data.</param>
        private void OnError(object seneder, ErrorEventArgs args)
        {
            Close();
            ShowMessageBoxAsync(args.ErrorMessage);
        }

        /// <summary>
        /// Called when [connected].
        /// </summary>
        private void OnConnected()
        {
            ShowMessageBoxAsync("Присоединен");
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public override void Close()
        {
            if( NetworkResearch != null )
            {
                try
                {
                    NetworkResearch.Connected -= OnConnected;
                    NetworkResearch.Error -= OnError;
                    NetworkResearch.ParametersCoordinated -= NetworkResearchOnParametersCoordinated;
                    NetworkResearch.TestComplete -= NetworkResearchOnTestComplete;
                    NetworkResearch.ResearchComplete -= NetworkResearchOnResearchComplete;
                    NetworkResearch.Dispose();
                }
                catch
                {
                }

            }

            if( netThread != null )
            {
                try
                {
                    netThread.Abort();
                }
                catch
                {

                }
            }
            IsProcessStarted = false;
        }
    }
}