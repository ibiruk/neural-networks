﻿using System;
using System.Collections.Generic;
using System.Linq;
using NeuralNetworksInfrastructure.Enum;
using NeuralNetworksInfrastructure.Helpers;

namespace NeuralNetworksUI.Bases
{
    public abstract class ConfigurationScreenBase : ScreenBase
    {
        private NetworkConfig _configuration;
        private int _countOfTests;
        private int _doneTest;

        protected override void InitializeDefaultValues()
        {
            Configuration = new NetworkConfig();
            CountOfTests = 1;
            DoneTestsCount = 0;
        }


        public NetworkConfig Configuration
        {
            get { return _configuration; }
            set
            {
                if (Equals(value, _configuration)) return;
                _configuration = value;
                NotifyOfPropertyChange();
            }
        }

        public int CountOfTests
        {
            get { return _countOfTests; }
            set
            {
                if (value == _countOfTests) return;
                _countOfTests = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange(nameof(ProgressTooltip));
            }
        }

        public int DoneTestsCount
        {
            get { return _doneTest; }
            set
            {
                if (value == _doneTest) return;
                _doneTest = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange(nameof(ProgressTooltip));
            }
        }

        /// <summary>
        /// Gets the progress tooltip.
        /// </summary>
        /// <value>
        /// The progress tooltip.
        /// </value>
        public string ProgressTooltip => $"{DoneTestsCount}/{CountOfTests}";

        public List<ETypesOfTPM> ValuesTypes
        {
            get { return Enum.GetValues(typeof(ETypesOfTPM)).Cast<ETypesOfTPM>().ToList(); }
        }
    }
}