﻿using System.Threading;
using NeuralNetworks.NetworkReserch;
using NeuralNetworksInfrastructure.EventArgs;

namespace NeuralNetworksUI.Bases
{
    public abstract class LocalScreenBase : ConfigurationScreenBase
    {
        private bool _isProcessStarted;
        protected Thread netThread;
        private bool _isAttemptsMustBeLoggedInDb = true;


        public bool IsAttemptsMustBeLoggedInDb
        {
            get { return _isAttemptsMustBeLoggedInDb; }
            set
            {
                if (value == _isAttemptsMustBeLoggedInDb) return;
                _isAttemptsMustBeLoggedInDb = value;
                NotifyOfPropertyChange(() => IsAttemptsMustBeLoggedInDb);
            }
        }

        /// <summary>
        ///     Gets or sets the network research.
        /// </summary>
        /// <value>
        ///     The network research.
        /// </value>
        protected LocalReserch LocalReserch { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is process started.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is process started; otherwise, <c>false</c>.
        /// </value>
        public bool IsProcessStarted
        {
            get { return _isProcessStarted; }
            set
            {
                if (value == _isProcessStarted) return;
                _isProcessStarted = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("IsCanBeEdited");
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is can be edited.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is can be edited; otherwise, <c>false</c>.
        /// </value>
        public bool IsCanBeEdited => IsProcessStarted == false;


        /// <summary>
        ///     Connects this instance.
        /// </summary>
        public void Start()
        {
            DoneTestsCount = 0;
            LocalReserch = new LocalReserch(100000, CountOfTests, Configuration, IsAttemptsMustBeLoggedInDb);
            LocalReserch.Error += OnError;
            LocalReserch.TestComplete += NetworkResearchOnTestComplete;
            LocalReserch.ResearchComplete += NetworkResearchOnResearchComplete;

            ThreadPool.QueueUserWorkItem(async state => await LocalReserch.RunResearch());
        }


        /// <summary>
        ///     Networks the research on research complete.
        /// </summary>
        private void NetworkResearchOnResearchComplete()
        {
            ShowMessageBoxAsync("Завершено");
            Close();
        }

        /// <summary>
        ///     Networks the research on test complete.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The arguments.</param>
        private void NetworkResearchOnTestComplete(object sender, TestCompleteArgs args)
        {
            DoneTestsCount++;
        }


        /// <summary>
        ///     Called when [error].
        /// </summary>
        /// <param name="seneder">The seneder.</param>
        /// <param name="args">The <see cref="ErrorEventArgs" /> instance containing the event data.</param>
        private void OnError(object seneder, ErrorEventArgs args)
        {
            Close();
            ShowMessageBoxAsync(args.ErrorMessage);
        }


        /// <summary>
        ///     Closes this instance.
        /// </summary>
        public override void Close()
        {
            if (LocalReserch != null)
                try
                {
                    LocalReserch.Error -= OnError;
                    LocalReserch.TestComplete -= NetworkResearchOnTestComplete;
                    LocalReserch.ResearchComplete -= NetworkResearchOnResearchComplete;
                }
                catch
                {
                }

            if (netThread != null)
                try
                {
                    netThread.Abort();
                }
                catch
                {
                }
            IsProcessStarted = false;
        }
    }
}