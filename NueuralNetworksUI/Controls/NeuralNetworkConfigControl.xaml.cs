﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NeuralNetworksInfrastructure.Helpers;

namespace NueuralNetworksUI.Controls
{
    /// <summary>
    /// Interaction logic for NeuralNetworkConfig.xaml
    /// </summary>
    public partial class NeuralNetworkConfigControl : UserControl
    {
        public NeuralNetworkConfigControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ConfigurationProperty = 
            DependencyProperty.Register("Configuration", typeof(NetworkConfig), typeof(NeuralNetworkConfigControl), new UIPropertyMetadata(new NetworkConfig()));

        public NetworkConfig Configuration
        {
            get { return (NetworkConfig)this.GetValue(ConfigurationProperty); }
            set { this.SetValue(ConfigurationProperty, value); }
        }

       
    }
}
