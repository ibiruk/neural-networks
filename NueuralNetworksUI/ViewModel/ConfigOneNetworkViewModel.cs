﻿using System.Windows;
using NeuralNetworksUI.Bases;

namespace NeuralNetworksUI
{
    public sealed class ConfigOneNetworkViewModel : NetworkScreenBase
    {
        public ConfigOneNetworkViewModel()
        {
            DisplayName = "Network One";
        }


        protected override void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}