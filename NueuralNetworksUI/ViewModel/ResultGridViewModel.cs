﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using Caliburn.Micro;
using MathNet.Numerics.Statistics;
using NeuralNetworksResults.Context;
using NeuralNetworksUI.Bases;

namespace NeuralNetworksUI
{
    public sealed class ResultGridViewModel : ScreenBase
    {
        protected WindowManager _windowManager { get; set; } = new WindowManager();

        private ObservableCollection<ShortResult> _result;

        public ObservableCollection<ShortResult> Result
        {
            get { return _result; }
            set
            {
                if (Equals(value, _result)) return;
                _result = value;
                NotifyOfPropertyChange();
            }
        }

        public ResultGridViewModel()
        {
            DisplayName = "Results Grid";
        }

        protected override void ShowMessage(string strMessage)
        {
            throw new NotImplementedException();
        }

        public void ShowGraph(ShortResult arg)
        {
            dynamic settings = new ExpandoObject();
            settings.MinWidth = 600;
            settings.MinHeight = 1000;
            _windowManager.ShowWindow(new ResearchBarChartViewModel(arg), null, settings);
        }


        public void OnLoaded()
        {
            using (var context = new NeuralNetworksResultDBContext())
            {
                var result = context.TestResults.Join(context.Researches,
                    tr => tr.ResearchId,
                    res => res.Id,
                    (res, research) => new
                    {
                        research.Id,
                        research.CountOfPerceptonNodes,
                        research.CountOfPerceptrons,
                        research.WeightLimit,
                        TypeOfValues = research.TypeOfValues.ToString(),
                        res.IsSyncSucceed,
                        res.SyncOperationsCount
                    }).ToList().GroupBy(x => new { x.CountOfPerceptonNodes, x.CountOfPerceptrons, x.WeightLimit, x.TypeOfValues }, arg => arg, (key, res) =>
                    {
                        var gr = res.Where(group => group.IsSyncSucceed).Select(x=>(double)x.SyncOperationsCount);
                        var stats = new DescriptiveStatistics(gr);
                        return new ShortResult
                        {
                            CountOfPerceptonNodes = key.CountOfPerceptonNodes,
                            CountOfPerceptrons = key.CountOfPerceptrons,
                            WeightLimit = key.WeightLimit,
                            TypeOfValues = key.TypeOfValues,
                            CountOfTests = res.Count(),
                            CountOfSuccess = gr.Count(),
                            AverageSyncOperationsCount = stats.Mean,
                            MinSyncOperationsCount = stats.Minimum,
                            MaxSyncOperationsCount = stats.Maximum,
                            StandardDeviation = stats.StandardDeviation
                        };
                    }).Where(res => res.CountOfTests > 500);

                Result = new ObservableCollection<ShortResult>(result);
            }
        }
    }
    

    public struct ShortResult
    {
        public string TypeOfValues { get; set; }

        public int CountOfPerceptonNodes { get; set; }

        public int CountOfPerceptrons { get; set; }

        public int WeightLimit { get; set; }

        public int CountOfTests { get; set; }

        public int CountOfSuccess { get; set; }

        public double MinSyncOperationsCount { get; set; }

        public double MaxSyncOperationsCount { get; set; }

        public double AverageSyncOperationsCount { get; set; }
        
        public double StandardDeviation { get; set; }
    }
}