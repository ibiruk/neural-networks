﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using NeuralNetworks.NeuralNetwork;
using NeuralNetworksInfrastructure.Enum;
using NeuralNetworksResults.Context;
using NeuralNetworksResults.Models;
using NeuralNetworksResults.NeuralNetworksResult;
using NeuralNetworksUI.Bases;

namespace NeuralNetworksUI
{
    public sealed class ConfigOneViewModel : ConfigurationScreenBase
    {
        public ConfigOneViewModel()
        {
            DisplayName = "Localhost One";


        }

        #region Local Methods

        public void PerformSimpleTest()
        {

            try
            {
                var tpmA = TPM.GetTPM( Configuration );
                var tpmB = TPM.GetTPM( Configuration );

                ThreadPool.QueueUserWorkItem( state => SimpleTest( tpmA, tpmB ) );

            }
            catch( InvalidEnumArgumentException enumArgument )
            {
                ShowMessageBoxAsync( enumArgument.Message );
            }
            catch( NotImplementedException notImplementedException )
            {
                ShowMessageBoxAsync( notImplementedException.Message );
            }


        }

        private void SimpleTest(TPM tpmA, TPM tpmB)
        {
            IsSimpleProcessStart = true;

            var outs = new List<string>();
            IEnumerable<IEnumerable<string>> inputs = new IEnumerable<string>[0];

            var tpmLists = new List<TPM>{tpmA, tpmB};
            int successivelyIterations = 0, maxSuccessivelyIterations = 0, lastcc = 0, siniteration = 0, syncCount = 0;
            var isSunck = false;
            while( true )
            {
                if( outs.Any() == false || outs[0] != outs[1] )
                {
                    //2. Generate random input vector X
                    inputs = tpmLists.First().GenerateInput( tpmLists.First().GetNetworkConfiguration() );
                }

                //4. Compute the value of the output neuron
                outs = tpmLists.AsParallel().Select( x => x.GetOutput( inputs ) ).ToList();

                var weights = tpmLists.AsParallel().Select( x => x.GetTPMWeights() ).ToList();
                int cc;
                //4. Compare the values of both tree parity machines
                var isEqual = TPM.CompareWeights(Configuration, weights[0], weights[1]);

                if(outs[0] == outs[1])
                {
                    siniteration++;
                }
                if( isEqual || siniteration == 100000 )
                {
                    if(isEqual)
                        isSunck = true;
                    break;
                }

                if( outs[0] == outs[1] )
                {
                    //Outputs are same: one of the suitable learning rules is applied to the weights
                    successivelyIterations++;
                    syncCount++;
                    tpmLists.AsParallel().ForAll( x => x.Synchronize() );
                }
                else
                {
                    //Outputs are different: go to 2
                    maxSuccessivelyIterations = Math.Max( maxSuccessivelyIterations, successivelyIterations );
                    successivelyIterations = 0;
                }
            }

            IsSimpleProcessStart = false;

            NetworkAResults = tpmLists.First().GetTPMWeights();
            NetworkBResults = tpmLists.Last().GetTPMWeights();
            ShowMessageBoxAsync( isSunck
                ? $"Успешно. \nПодряд last:{successivelyIterations} max:{maxSuccessivelyIterations}\n Всего:{syncCount}"
                : "Синхронизация не удалась" );
        }

        #endregion

        #region Properties

        private IEnumerable<IEnumerable<string>> _networkAResults;
        private IEnumerable<IEnumerable<string>> _networkBResults;
        private bool _isSimpleProcessStart;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is simple process start.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is simple process start; otherwise, <c>false</c>.
        /// </value>
        public bool IsSimpleProcessStart
        {
            get { return _isSimpleProcessStart; }
            set
            {
                if (value == _isSimpleProcessStart) return;
                _isSimpleProcessStart = value;
                NotifyOfPropertyChange();
            }
        }
        public IEnumerable<IEnumerable<string>> NetworkAResults
        {
            get { return _networkAResults; }
            set
            {
                if (Equals(value, _networkAResults)) return;
                _networkAResults = value;
                NotifyOfPropertyChange();
            }
        }

        public IEnumerable<IEnumerable<string>> NetworkBResults
        {
            get { return _networkBResults; }
            set
            {
                if (Equals(value, _networkBResults)) return;
                _networkBResults = value;
                NotifyOfPropertyChange();
            }
        }

        protected override void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        #endregion
    }
}