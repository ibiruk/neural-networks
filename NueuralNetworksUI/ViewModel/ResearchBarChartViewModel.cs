﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using MathNet.Numerics.Distributions;
using NeuralNetworksResults.Context;
using MathNet.Numerics.Statistics;

namespace NeuralNetworksUI
{
    public sealed class ResearchBarChartViewModel : Screen
    {


        private Dictionary<string, double> _data;
        private Dictionary<string, double> _generatedData;
        private double _average;
        private double _standardDeviation;
        private double _tripleSd;
        private double _valuesInTripleSd;
        private string _normalStr;

        private string TypeOfValues { get; set; }

        private int CountOfPerceptonNodes { get; set; }

        private int CountOfPerceptrons { get; set; }

        private int WeightLimit { get; set; }

        public Dictionary<string, double> Data
        {
            get { return _data; }
            set
            {
                if (Equals(value, _data)) return;
                _data = value;
                NotifyOfPropertyChange();
            }
        }

        public Dictionary<string, double> GeneratedData
        {
            get { return _generatedData; }
            set
            {
                if (Equals(value, _data)) return;
                _generatedData = value;
                NotifyOfPropertyChange();
            }
        }

        public double Average
        {
            get { return _average; }
            set
            {
                if (value.Equals(_average)) return;
                _average = value;
                NotifyOfPropertyChange();
            }
        }

        public double StandardDeviation
        {
            get { return _standardDeviation; }
            set
            {
                if (value.Equals(_standardDeviation)) return;
                _standardDeviation = value;
                NotifyOfPropertyChange();
            }
        }

        public double TripleSD
        {
            get { return _tripleSd; }
            set
            {
                if (value.Equals(_tripleSd)) return;
                _tripleSd = value;
                NotifyOfPropertyChange();
            }
        }


        public double ValuesInTripleSD
        {
            get { return _valuesInTripleSd; }
            set
            {
                if (value == _valuesInTripleSd) return;
                _valuesInTripleSd = value;
                NotifyOfPropertyChange();
            }
        }

        public string NormalStr
        {
            get { return _normalStr; }
            set
            {
                if (value == _normalStr) return;
                _normalStr = value;
                NotifyOfPropertyChange();
            }
        }

        private ResearchBarChartViewModel() { }

        public ResearchBarChartViewModel(ShortResult result)
        {
            TypeOfValues = result.TypeOfValues;
            CountOfPerceptrons = result.CountOfPerceptrons;
            CountOfPerceptonNodes = result.CountOfPerceptonNodes;
            WeightLimit = result.WeightLimit;

            this.DisplayName = $"{TypeOfValues} : " +
                               $"K*N ={CountOfPerceptrons}*{CountOfPerceptonNodes}; " +
                               $"W -{WeightLimit},...,0,...,+{WeightLimit}";
        }

        public void OnLoaded()
        {
            Data = new Dictionary<string, double>();
            using (var context = new NeuralNetworksResultDBContext())
            {
                var data = context.TestResults.Join(context.Researches,
                    tr => tr.ResearchId,
                    res => res.Id,
                    (res, research) => new
                    {
                        research.CountOfPerceptonNodes,
                        research.CountOfPerceptrons,
                        research.WeightLimit,
                        TypeOfValues = research.TypeOfValues.ToString(),
                        res.IsSyncSucceed,
                        res.SyncOperationsCount
                    }).Where(x =>
                    x.CountOfPerceptonNodes == CountOfPerceptonNodes
                    && x.CountOfPerceptrons == CountOfPerceptrons
                    && x.WeightLimit == WeightLimit
                    && x.TypeOfValues == TypeOfValues
                    && x.IsSyncSucceed).Select(x => (double)x.SyncOperationsCount);

                var histogram = new Histogram(data.ToList(), 30);


                for (var i = 0; i < histogram.BucketCount; i++)
                {
                    var bucket = histogram[i];
                    Data.Add($"{bucket.LowerBound:N0} - {bucket.UpperBound:N0}", bucket.Count);
                }

                
                var stats = new DescriptiveStatistics(data.Select(x => x));

                Average = stats.Mean;
                StandardDeviation = stats.StandardDeviation;
                TripleSD = Average + 3 * StandardDeviation;

                var countInTripleSDRange = data.Count(x =>
                        x <= TripleSD &&
                        x >= Average - 3 * StandardDeviation);

                ValuesInTripleSD = Math.Round((double)countInTripleSDRange / data.Count() * 100, 2);


                var normal = new Normal(stats.Mean, stats.StandardDeviation);

                var values = new double[10000];
                normal.Samples(values);


                GeneratedData = new Dictionary<string, double>();
                for (var i = 0; i < histogram.BucketCount; i++)
                {
                    var bucket = histogram[i];
                    var count = values.Count(x => x >= bucket.LowerBound && x < bucket.UpperBound);

                    GeneratedData.Add($"{bucket.LowerBound:N0} - {bucket.UpperBound:N0}", count);
                }

                NormalStr = normal.ToString();

            }
        }
    }
}
