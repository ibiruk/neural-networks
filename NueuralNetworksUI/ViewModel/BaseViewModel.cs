﻿using System;
using System.Linq;
using Caliburn.Micro;
using NeuralNetworksUI.Bases;
using NueuralNetworksUI.Helpers;

namespace NeuralNetworksUI
{
    public sealed class BaseViewModel : Conductor<ScreenBase>.Collection.OneActive
    {
        public BaseViewModel():base()
        {
            DisplayName = "Neural networks";
            Items.Add(ScreenFactory<ConfigOneViewModel>.GetScreen());
            Items.Add(ScreenFactory<ConfigManyLocalViewModel>.GetScreen());
            Items.Add(ScreenFactory<ConfigOneNetworkViewModel>.GetScreen());
            Items.Add(ScreenFactory<ConfigManyNetworkViewModel>.GetScreen());
            Items.Add(new ResultGridViewModel());
            ActivateItem( Items.First() );
        }

        public void OnClose(EventArgs args)
        {
            foreach(var screen in Items)
            {
                screen.Close(  );
            }
        }
    }
}
