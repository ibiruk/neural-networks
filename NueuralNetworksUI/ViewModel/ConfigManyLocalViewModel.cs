﻿using System.Windows;
using NeuralNetworksUI.Bases;

namespace NeuralNetworksUI
{
    public sealed class ConfigManyLocalViewModel : LocalScreenBase
    {

        public ConfigManyLocalViewModel()
        {
            DisplayName = "Local Research";
        }

        protected override void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}