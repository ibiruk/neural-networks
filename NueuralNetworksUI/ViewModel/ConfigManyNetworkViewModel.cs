﻿using System.Windows;
using NeuralNetworksUI.Bases;

namespace NeuralNetworksUI
{
    public sealed class ConfigManyNetworkViewModel : NetworkScreenBase
    {

        public ConfigManyNetworkViewModel()
        {
            DisplayName = "Network Research";
        }

        protected override void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}