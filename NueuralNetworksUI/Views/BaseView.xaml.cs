﻿using System.Windows;

namespace NeuralNetworksUI
{
    /// <summary>
    ///     Interaction logic for ConfigView.xaml
    /// </summary>
    public partial class BaseView : Window
    {
        public BaseView()
        {
            InitializeComponent();
        }
    }
}