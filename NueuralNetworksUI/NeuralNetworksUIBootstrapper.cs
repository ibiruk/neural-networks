﻿using System.Globalization;
using System.Threading;
using System.Windows;
using Caliburn.Micro;
using NueuralNetworksUI;

namespace NeuralNetworksUI
{
    public class NeuralNetworksUIBootstrapper : BootstrapperBase
    {
        public NeuralNetworksUIBootstrapper() : base()
        {
            Initialize();
        }
        
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            

            log4net.Config.XmlConfigurator.Configure();
            DisplayRootViewFor<BaseViewModel>();

            Thread.CurrentThread.CurrentUICulture = 
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("ru-RU");
        }

    }
}