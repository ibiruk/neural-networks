﻿using System;
using System.Collections.Generic;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworksInfrastructure.Algebra
{
    public class NumberSystemBase<T>: INumberSystem<T> 
    {
        protected bool Equals(NumberSystemBase<T> other)
        {
            return Real.Equals( other.Real );
        }

        public virtual INumberSystem<T> GetIdentity()
        {
            throw new NotImplementedException();
        }

        public bool IsIdentity => this == GetIdentity();
        public double Real { get; set; }

        public virtual IEnumerable<double> GetComponents()
        {
            throw new NotImplementedException();
        }
    }
}
