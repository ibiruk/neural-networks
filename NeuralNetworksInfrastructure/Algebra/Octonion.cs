﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworksInfrastructure.Algebra
{
    /// <summary>
    ///     This class provides methods and structures to describe a mathematical octonion.
    /// </summary>
    [Serializable]
    public class Octonion : NumberSystemBase<Octonion>
    {
        private static readonly Octonion Identity = new Octonion( 1 );

        /// <summary>
        ///     Initializes a new instance of the <see cref="Octonion" /> class.
        /// </summary>
        public Octonion()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Octonion" /> class.
        /// </summary>
        /// <param name="realPart">The real part.</param>
        public Octonion(double realPart)
        {
            Real = realPart;
            I = J = K = L = IL = JL = KL = 0;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Octonion" /> class.
        /// </summary>
        /// <param name="realPart">The real part.</param>
        /// <param name="i">The i.</param>
        /// <param name="j">The j.</param>
        /// <param name="k">The k.</param>
        /// <param name="l">The l.</param>
        /// <param name="il">The il.</param>
        /// <param name="jl">The jl.</param>
        /// <param name="kl">The kl.</param>
        public Octonion(double realPart, double i, double j, double k, double l, double il, double jl, double kl)
        {
            Real = realPart;
            I = i;
            J = j;
            K = k;
            L = l;
            IL = il;
            JL = jl;
            KL = kl;
        }

        /// <summary>
        ///     Gets or sets the i part of the octonion.
        /// </summary>
        /// <value>
        ///     The i part of the octonion.
        /// </value>
        public double I { get; set; }

        /// <summary>
        /// </summary>
        /// <value>
        ///     The j part of the octonion.
        /// </value>
        public double J { get; set; }

        /// <summary>
        /// </summary>
        /// <value>
        ///     The k part of the octonion.
        /// </value>
        public double K { get; set; }

        /// <summary>
        /// </summary>
        /// <value>
        ///     The l part of the octonion.
        /// </value>
        public double L { get; set; }

        /// <summary>
        ///     Gets or sets the il part of the octonion.
        /// </summary>
        /// <value>
        ///     The il part of the octonion.
        /// </value>
        public double IL { get; set; }

        /// <summary>
        /// </summary>
        /// <value>
        ///     The jl part of the octonion.
        /// </value>
        public double JL { get; set; }

        /// <summary>
        /// </summary>
        /// <value>
        ///     The kl part of the octonion.
        /// </value>
        public double KL { get; set; }

        protected bool Equals(Octonion other)
        {
            return base.Equals( other ) && I.Equals( other.I ) && J.Equals( other.J ) && K.Equals( other.K ) && L.Equals( other.L ) && IL.Equals( other.IL ) && JL.Equals( other.JL ) &&
                   KL.Equals( other.KL );
        }

        public override bool Equals(object obj)
        {
            if( ReferenceEquals( null, obj ) ) return false;
            if( ReferenceEquals( this, obj ) ) return true;
            if( obj.GetType() != GetType() ) return false;
            return Equals( (Octonion)obj );
        }

        public override INumberSystem<Octonion> GetIdentity()
        {
            return Identity;
        }

        /// <summary>
        ///     Copies the current instance of an octonion.
        /// </summary>
        /// <returns>
        ///     The copy of the current instance.
        /// </returns>
        public Octonion Copy()
        {
            return new Octonion( Real, I, J, K, L, IL, JL, KL );
        }

        /// <summary>
        ///     Compares the current instance of an octonion to another instance.
        /// </summary>
        /// <param name="compare">The octonion to compare.</param>
        /// <returns>
        ///     True if both octonions are even otherwise, false.
        /// </returns>
        public bool CompareTo(Octonion compare)
        {
            return this == compare;
        }

        /// <summary>
        ///     Powers the current sedenion raised to the specified power.
        /// </summary>
        /// <param name="value">The value to raised with.</param>
        /// <returns>
        ///     The new sedenion raised to the specified power.
        /// </returns>
        public Octonion Pow(double value)
        {
            var result = new Octonion();

            result.Real = Math.Pow( Real, value );
            result.I = Math.Pow( I, value );
            result.J = Math.Pow( J, value );
            result.K = Math.Pow( K, value );
            result.L = Math.Pow( L, value );
            result.IL = Math.Pow( IL, value );
            result.JL = Math.Pow( JL, value );
            result.KL = Math.Pow( KL, value );

            return result;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Real.GetHashCode();
                hashCode = hashCode ^ I.GetHashCode();
                hashCode = hashCode ^ J.GetHashCode();
                hashCode = hashCode ^ K.GetHashCode();
                hashCode = hashCode ^ L.GetHashCode();
                hashCode = hashCode ^ IL.GetHashCode();
                hashCode = hashCode ^ JL.GetHashCode();
                hashCode = hashCode ^ KL.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        ///     Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        ///     The result of the operator.
        /// </returns>
        public static bool operator ==(Octonion a, Octonion b)
        {
            if( ReferenceEquals( a, b ) )
                return true;

            if( ( (object)a == null ) || ( (object)b == null ) )
                return false;

            return ( a.Real == b.Real ) && ( a.I == b.I ) && ( a.J == b.J ) && ( a.K == b.K )
                   && ( a.L == b.L ) && ( a.IL == b.IL ) && ( a.JL == b.JL ) && ( a.KL == b.KL );
        }

        /// <summary>
        ///     Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        ///     The result of the operator.
        /// </returns>
        public static bool operator !=(Octonion a, Octonion b)
        {
            return !( a == b );
        }

        /// <summary>
        ///     Implements the operator +.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        ///     The result of the operator.
        /// </returns>
        public static Octonion operator +([NotNull] Octonion a, [NotNull] Octonion b)
        {
            return new Octonion{
                Real = a.Real + b.Real,
                I = a.I + b.I,
                J = a.J + b.J,
                K = a.K + b.K,
                L = a.L + b.L,
                IL = a.IL + b.IL,
                JL = a.JL + b.JL,
                KL = a.KL + b.KL
            };
        }

        /// <summary>
        ///     Implements the operator -.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        ///     The result of the operator.
        /// </returns>
        public static Octonion operator -([NotNull] Octonion a, [NotNull] Octonion b)
        {
            return new Octonion{
                Real = a.Real - b.Real,
                I = a.I - b.I,
                J = a.J - b.J,
                K = a.K - b.K,
                L = a.L - b.L,
                IL = a.IL - b.IL,
                JL = a.JL - b.JL,
                KL = a.KL - b.KL
            };
            
        }

        /// <summary>
        ///     Implements the operator *.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///     The result of the operator.
        /// </returns>
        public static Octonion operator *([NotNull] Octonion a, double value)
        {
            return new Octonion{
                Real = a.Real*value,
                I = a.I*value,
                J = a.J*value,
                K = a.K*value,
                L = a.L*value,
                IL = a.IL*value,
                JL = a.JL*value,
                KL = a.KL*value
            };
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Octonion operator *(Octonion a, Octonion b)
        {
            return new Octonion(
                a.Real*b.Real - a.I*b.I - a.J*b.J - a.K*b.K - a.L*b.L - a.IL*b.IL - a.JL*b.JL - a.KL*b.KL,
                a.Real*b.I + a.I*b.Real - a.J*b.K + a.K*b.J - a.L*b.IL + a.IL*b.L + a.JL*b.KL - a.KL*b.JL,
                a.Real*b.J + a.I*b.K + a.J*b.Real - a.K*b.I - a.L*b.JL - a.IL*b.KL + a.JL*b.L + a.KL*b.IL,
                a.Real*b.K - a.I*b.J + a.J*b.I + a.K*b.Real - a.L*b.KL + a.IL*b.JL - a.JL*b.IL + a.KL*b.L,
                a.Real*b.L + a.I*b.IL + a.J*b.JL + a.K*b.KL + a.L*b.Real - a.IL*b.I - a.JL*b.J - a.KL*b.K,
                a.Real*b.IL - a.I*b.L + a.J*b.KL - a.K*b.JL + a.L*b.I + a.IL*b.Real + a.JL*b.K - a.KL*b.J,
                a.Real*b.JL - a.I*b.KL - a.J*b.L + a.K*b.IL + a.L*b.J - a.IL*b.K + a.JL*b.Real + a.KL*b.I,
                a.Real*b.KL + a.I*b.JL - a.J*b.IL - a.K*b.L + a.L*b.K + a.IL*b.J - a.JL*b.I + a.KL*b.Real
            );
        }


        /// <summary>
        /// Parses the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static Octonion Parse(string source)
        {
            var tokenizerHelper = new SimpleTokenizerHelper( source );

            var octonion = source == "Identity"
                ? Identity
                : new Octonion(
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ) );


            return octonion;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if( IsIdentity )
                return "Identity";
            var numericListSeparator = SimpleTokenizerHelper.ArgSeparator;
            return $"{Real}{numericListSeparator}" +
                   $"{I}{numericListSeparator}" +
                   $"{J}{numericListSeparator}" +
                   $"{K}{numericListSeparator}" +
                   $"{L}{numericListSeparator}" +
                   $"{IL}{numericListSeparator}" +
                   $"{JL}{numericListSeparator}" +
                   $"{KL}";
        }

        /// <summary>
        /// Gets the components.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<double> GetComponents()
        {
            return new[]{Real, I, J, K, L, IL, JL, KL};
        }
    }
}