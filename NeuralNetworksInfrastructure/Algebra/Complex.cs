﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworksInfrastructure.Algebra
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="NeuralNetworksInfrastructure.Algebra.NumberSystemBase{NeuralNetworksInfrastructure.Algebra.Complex}" />
    [Serializable]
    public class Complex : NumberSystemBase<Complex>
    {
        private static readonly Complex Identity = new Complex( 1 );

        /// <summary>
        /// Initializes a new instance of the <see cref="Complex"/> class.
        /// </summary>
        public Complex()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Complex"/> class.
        /// </summary>
        /// <param name="realPart">The real part.</param>
        public Complex(double realPart)
        {
            Real = realPart;
            I = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Complex"/> class.
        /// </summary>
        /// <param name="realPart">The real part.</param>
        /// <param name="i">The i.</param>
        public Complex(double realPart, double i)
        {
            Real = realPart;
            I = i;
        }

        /// <summary>
        /// Gets or sets the i.
        /// </summary>
        /// <value>
        /// The i.
        /// </value>
        public double I { get; set; }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        protected bool Equals(Complex other)
        {
            return base.Equals( other ) && I.Equals( other.I );
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if( ReferenceEquals( null, obj ) ) return false;
            if( ReferenceEquals( this, obj ) ) return true;
            if( obj.GetType() != GetType() ) return false;
            return Equals( (Complex)obj );
        }

        /// <summary>
        /// Gets the identity.
        /// </summary>
        /// <returns></returns>
        public override INumberSystem<Complex> GetIdentity()
        {
            return Identity;
        }

        /// <summary>
        /// Copies this instance.
        /// </summary>
        /// <returns></returns>
        public Complex Copy()
        {
            return new Complex( Real, I );
        }

        /// <summary>
        /// Compares to.
        /// </summary>
        /// <param name="compare">The compare.</param>
        /// <returns></returns>
        public bool CompareTo(Complex compare)
        {
            return this == compare;
        }

        /// <summary>
        /// Pows the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public Complex Pow(double value)
        {
            return new Complex{
                Real = Math.Pow( Real, value ),
                I = Math.Pow( I, value )
            };
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Real.GetHashCode();
                hashCode = hashCode ^ I.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(Complex a, Complex b)
        {
            if( ReferenceEquals( a, b ) )
                return true;

            if( ( (object)a == null ) || ( (object)b == null ) )
                return false;

            return ( a.Real == b.Real ) && ( a.I == b.I );
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(Complex a, Complex b)
        {
            return !( a == b );
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Complex operator *(Complex a, Complex b)
        {
            return new Complex(
                a.Real*b.Real - a.I*b.I,
                a.Real*b.I + a.I*b.Real );
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Complex operator +([NotNull] Complex a, [NotNull] Complex b)
        {
            return new Complex{
                Real = a.Real + b.Real,
                I = a.I + b.I
            };
        }

        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Complex operator -([NotNull] Complex a, [NotNull] Complex b)
        {
            return new Complex{
                Real = a.Real - b.Real,
                I = a.I - b.I
            };
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Complex operator *([NotNull] Complex a, double value)
        {
            return new Complex{
                Real = a.Real*value,
                I = a.I*value
            };
        }

        /// <summary>
        /// Parses the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static Complex Parse(string source)
        {
            var tokenizerHelper = new SimpleTokenizerHelper( source );

            var octonion = source == "Identity"
                ? Identity
                : new Complex(
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ) );


            return octonion;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if( IsIdentity )
                return "Identity";
            var numericListSeparator = SimpleTokenizerHelper.ArgSeparator;
            return $"{Real}{numericListSeparator}" +
                   $"{I}";
        }

        /// <summary>
        /// Deqs this instance.
        /// </summary>
        /// <returns></returns>
        public double Deq()
        {
            var phase = Math.Atan2( I, Real ); // на с++ arg(output)
            return Math.Abs( phase*180/Math.PI ) + 180;
        }

        /// <summary>
        /// Gets the components.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<double> GetComponents()
        {
            return new[]{Real, I};
        }
    }
}