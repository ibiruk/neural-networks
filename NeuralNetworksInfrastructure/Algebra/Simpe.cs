﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworksInfrastructure.Algebra
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="NeuralNetworksInfrastructure.Algebra.NumberSystemBase{NeuralNetworksInfrastructure.Algebra.Simple}" />
    [Serializable]
    public class Simple : NumberSystemBase<Simple>
    {
        private static readonly Simple Identity = new Simple( 1 );

        /// <summary>
        /// Initializes a new instance of the <see cref="Simple"/> class.
        /// </summary>
        public Simple()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Simple"/> class.
        /// </summary>
        /// <param name="realPart">The real part.</param>
        public Simple(double realPart)
        {
            Real = realPart;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if( ReferenceEquals( null, obj ) ) return false;
            if( ReferenceEquals( this, obj ) ) return true;
            if( obj.GetType() != GetType() ) return false;
            return Equals( (Simple)obj );
        }

        /// <summary>
        /// Gets the identity.
        /// </summary>
        /// <returns></returns>
        public override INumberSystem<Simple> GetIdentity()
        {
            return Identity;
        }

        /// <summary>
        /// Copies this instance.
        /// </summary>
        /// <returns></returns>
        public Simple Copy()
        {
            return new Simple( Real );
        }

        /// <summary>
        /// Compares to.
        /// </summary>
        /// <param name="compare">The compare.</param>
        /// <returns></returns>
        public bool CompareTo(Simple compare)
        {
            return this == compare;
        }

        /// <summary>
        /// Pows the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public Simple Pow(double value)
        {
            var result = new Simple();

            result.Real = Math.Pow( Real, value );

            return result;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Real.GetHashCode();
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(Simple a, Simple b)
        {
            return Math.Abs( a.Real - b.Real ) < Double.Epsilon;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(Simple a, Simple b)
        {
            return Math.Abs( a.Real - b.Real ) > Double.Epsilon;
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Simple operator +([NotNull] Simple a, [NotNull] Simple b)
        {
            return new Simple{Real = a.Real + b.Real};
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Simple operator *([NotNull] Simple a, [NotNull] Simple b)
        {
            return new Simple{Real = a.Real*b.Real};
        }

        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Simple operator -([NotNull] Simple a, [NotNull] Simple b)
        {
            return new Simple{Real = a.Real - b.Real};
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Simple operator *([NotNull] Simple a, double value)
        {
            return new Simple{Real = a.Real*value};
        }

        /// <summary>
        /// Parses the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static Simple Parse(string source)
        {
            var tokenizerHelper = new SimpleTokenizerHelper( source );

            var octonion = source == "Identity"
                ? Identity
                : new Simple( Convert.ToDouble( tokenizerHelper.NextTokenRequired() ) );


            return octonion;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return IsIdentity ? "Identity" : $"{Real}";
        }

        /// <summary>
        /// Gets the components.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<double> GetComponents()
        {
            return new[]{Real};
        }
    }
}