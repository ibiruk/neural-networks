﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using NeuralNetworksInfrastructure.Helpers;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworksInfrastructure.Algebra
{
    public class Quaternion : NumberSystemBase<Quaternion>
    {
        private static readonly Quaternion Identity = new Quaternion( 1 );

        /// <summary>
        /// Initializes a new instance of the <see cref="Quaternion"/> class.
        /// </summary>
        public Quaternion()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Quaternion"/> class.
        /// </summary>
        /// <param name="realPart">The real part.</param>
        public Quaternion(double realPart)
        {
            Real = realPart;
            I = J = K = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Quaternion"/> class.
        /// </summary>
        /// <param name="realPart">The real part.</param>
        /// <param name="i">The i.</param>
        /// <param name="j">The j.</param>
        /// <param name="k">The k.</param>
        public Quaternion(double realPart, double i, double j, double k)
        {
            Real = realPart;
            I = i;
            J = j;
            K = k;
        }


        /// <summary>
        /// Gets or sets the i.
        /// </summary>
        /// <value>
        /// The i.
        /// </value>
        public double I { get; set; }

        /// <summary>
        /// </summary>
        /// <value>
        /// The j.
        /// </value>
        public double J { get; set; }

        /// <summary>
        /// </summary>
        /// <value>
        /// The k.
        /// </value>
        public double K { get; set; }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        protected bool Equals(Quaternion other)
        {
            return base.Equals( other ) && I.Equals( other.I ) && J.Equals( other.J ) && K.Equals( other.K );
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if( ReferenceEquals( null, obj ) ) return false;
            if( ReferenceEquals( this, obj ) ) return true;
            if( obj.GetType() != GetType() ) return false;
            return Equals( (Quaternion)obj );
        }

        /// <summary>
        /// Gets the identity.
        /// </summary>
        /// <returns></returns>
        public override INumberSystem<Quaternion> GetIdentity()
        {
            return Identity;
        }

        /// <summary>
        /// Copies this instance.
        /// </summary>
        /// <returns></returns>
        public Quaternion Copy()
        {
            return new Quaternion( Real, I, J, K );
        }

        /// <summary>
        /// Compares to.
        /// </summary>
        /// <param name="compare">The compare.</param>
        /// <returns></returns>
        public bool CompareTo(Quaternion compare)
        {
            return this == compare;
        }

        /// <summary>
        /// Pows the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public Quaternion Pow(double value)
        {
            return new Quaternion{
                Real = Math.Pow( Real, value ),
                I = Math.Pow( I, value ),
                J = Math.Pow( J, value ),
                K = Math.Pow( K, value )
            };
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Real.GetHashCode();
                hashCode = hashCode ^ I.GetHashCode();
                hashCode = hashCode ^ J.GetHashCode();
                hashCode = hashCode ^ K.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        ///     Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        ///     The result of the operator.
        /// </returns>
        public static bool operator ==(Quaternion a, Quaternion b)
        {
            if( ReferenceEquals( a, b ) )
                return true;

            if( ( (object)a == null ) || ( (object)b == null ) )
                return false;

            return ( a.Real == b.Real ) && ( a.I == b.I ) && ( a.J == b.J ) && ( a.K == b.K );
        }

        /// <summary>
        ///     Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        ///     The result of the operator.
        /// </returns>
        public static bool operator !=(Quaternion a, Quaternion b)
        {
            return !( a == b );
        }

        public static Quaternion operator *(Quaternion left, Quaternion right)
        {
            return new Quaternion(
                left.Real*right.Real - left.I*right.I - left.J*right.J - left.K*right.K,
                left.Real*right.I + left.I*right.Real - left.J*right.K + left.K*right.J,
                left.Real*right.J + left.I*right.K + left.J*right.Real - left.K*right.I,
                left.Real*right.K - left.I*right.J + left.J*right.I + left.K*right.Real );
        }


        /// <summary>
        ///     Implements the operator +.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        ///     The result of the operator.
        /// </returns>
        public static Quaternion operator +([NotNull] Quaternion a, [NotNull] Quaternion b)
        {
            return new Quaternion{
                Real = a.Real + b.Real,
                I = a.I + b.I,
                J = a.J + b.J,
                K = a.K + b.K
            };
            
        }

        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Quaternion operator -([NotNull] Quaternion a, [NotNull] Quaternion b)
        {
            return new Quaternion{
                Real = a.Real - b.Real,
                I = a.I - b.I,
                J = a.J - b.J
            };
        }

        /// <summary>
        ///     Implements the operator *.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///     The result of the operator.
        /// </returns>
        public static Quaternion operator *([NotNull] Quaternion a, double value)
        {
            return new Quaternion{
                Real = a.Real*value,
                I = a.I*value,
                J = a.J*value,
                K = a.K*value
            };
        }

        /// <summary>
        /// Parses the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static Quaternion Parse(string source)
        {
            var tokenizerHelper = new SimpleTokenizerHelper( source );

            var octonion = source == "Identity"
                ? Identity
                : new Quaternion(
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ),
                    Convert.ToDouble( tokenizerHelper.NextTokenRequired() ) );


            return octonion;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if( IsIdentity )
                return "Identity";
            var numericListSeparator = SimpleTokenizerHelper.ArgSeparator;
            return $"{Real}{numericListSeparator}" +
                   $"{I}{numericListSeparator}" +
                   $"{J}{numericListSeparator}" +
                   $"{K}";
        }

        /// <summary>
        /// Gets the components.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<double> GetComponents()
        {
            return new[]{Real, I, J, K};
        }
    }
}