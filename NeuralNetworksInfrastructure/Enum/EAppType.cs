﻿namespace NeuralNetworksInfrastructure.Enum
{
    public enum EAppType
    {
        Server,
        Client,
        Undefined = 1000
    }
}