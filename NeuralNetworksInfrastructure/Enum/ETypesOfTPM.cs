﻿namespace NeuralNetworksInfrastructure.Enum
{
    public enum ETypesOfTPM
    {
        Int = 1,
        Complex = 2,

        Quaternion = 3,

        Octonion= 4
    }
}