using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using NeuralNetworksInfrastructure.Enum;
using NeuralNetworksInfrastructure.EventArgs;

namespace NeuralNetworksInfrastructure.Helpers
{
    public class SocketWorker : IDisposable
    {
        private bool _isDisposed;
        private static readonly ILog Log = LogManager.GetLogger( MethodBase.GetCurrentMethod().DeclaringType );

        protected Socket _handler; 
        protected IPAddress _ipAddr;
        protected IPEndPoint _ipEndPoint; 
        protected IPHostEntry _ipHost; 

        protected Socket _mainSocket; 
        private string _lastSend;
        private string _lastReceive;
        private readonly int SleepTime = 20;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is connected.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is connected; otherwise, <c>false</c>.
        /// </value>
        public bool IsConnected { get; set; }

        /// <summary>
        /// Prevents a default instance of the <see cref="SocketWorker"/> class from being created.
        /// </summary>
        private SocketWorker()
        {
            _lastSend = String.Empty;
            _lastReceive = String.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SocketWorker"/> class.
        /// </summary>
        /// <param name="hostName">Name of the host.</param>
        /// <param name="port">The port.</param>
        /// <param name="type">The type.</param>
        public SocketWorker(string hostName, int port, EAppType type) : this()
        {
            Host = hostName;
            Port = port;
            Type = type;
        }

        protected double TimeOut => TimeSpan.FromMinutes( 1 ).TotalMilliseconds;

        protected EAppType Type { get; set; }

        protected string Host { get; set; }
        protected int Port { get; set; }


        /// <summary>
        /// Closes this instance.
        /// </summary>
        public void Close()
        {
            try
            {
                if( Type.Equals( EAppType.Server ) )
                {
                    _handler.Disconnect( false );
                    _handler.Close();
                    _handler.Dispose();
                    if( _mainSocket.Connected )
                    {
                        _mainSocket.Disconnect( false );
                        _mainSocket.Shutdown( SocketShutdown.Both );
                    }
                    _mainSocket.Close();
                    _mainSocket.Dispose();
                }
                else
                {
                    _mainSocket.Disconnect( false );
                    _mainSocket.Close();
                    _mainSocket.Dispose();
                }
            }
            catch( Exception exception )
            {
                Log.Error( exception.Message, exception );
            }
        }

        /// <summary>
        /// Creates the socket.
        /// </summary>
        public void CreateSocket()
        {
            if( Type.Equals( EAppType.Server ) )
            {
                CreateServerSocket();
            }
            else
            {
                CreateClientSocket();
            }


        }

        /// <summary>
        ///     Gets the message.
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetMessageAsync()
        {
            _lastReceive = await GetInformationFromSocket( Type == EAppType.Client ? _mainSocket : _handler );
            return _lastReceive;
        }

        /// <summary>
        ///     Sends the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public async Task SendMessageAsync(string message)
        {

            _lastSend = message;
            Log.Info($"CompressAsync Started {message}");
            var sendMsg = await CompressUtility.CompressAsync( message );
            Log.Info("CompressAsync Started");
            SendMessageBySocket( sendMsg, Type == EAppType.Server ? _handler : _mainSocket );
        }

        /// <summary>
        ///     Gets the information from socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Time Out</exception>
        private async Task<string> GetInformationFromSocket(Socket socket)
        {
            Log.Info("GetInformationFromSocket Started");
            var runTime = Stopwatch.StartNew();
            var getInformation = string.Empty;
            while (socket.Poll(1, SelectMode.SelectRead) == false)
            {
                Sleep();
                if (runTime.ElapsedMilliseconds > TimeOut)
                {
                    throw new TimeoutException($"{Type} Time Out on Receive!!! last send= {_lastSend}, last receive = {_lastReceive}");
                }
            }

            runTime = Stopwatch.StartNew();
            while (socket.Available<=0)
            {
                Sleep();
                if (runTime.ElapsedMilliseconds > TimeOut)
                {
                    throw new TimeoutException($"{Type} Time Out on Receive!!! last send= {_lastSend}, last receive = {_lastReceive}");
                }
            }

            
            runTime = Stopwatch.StartNew();
            var resultBytes = new List<byte> ();
            do
            {
                if (runTime.ElapsedMilliseconds > TimeOut)
                {
                    throw new TimeoutException($"{Type} Time Out on Receive!!! last send= {_lastSend}, last receive = {_lastReceive}");
                }
                var getBytes = new byte[100]; // o_O
                var count = socket.Receive( getBytes );
                resultBytes.AddRange(getBytes.Take(count));
            } while(socket.Available>0);
            Log.Info("DecompressAsync Started");
            getInformation += await CompressUtility.DecompressAsync(resultBytes.ToArray());
            Log.Info($"DecompressAsync Ended {getInformation}");
            Log.Info($"GetInformationFromSocket Ended {getInformation}");
            return getInformation;
        }


        /// <summary>
        /// Sends the message by socket.
        /// </summary>
        /// <param name="sendMsg">The send MSG.</param>
        /// <param name="socket">The socket.</param>
        private void SendMessageBySocket(byte[] sendMsg, Socket socket)
        {
            Log.Info("SendMessageBySocket Started");
            Sleep();
            var runTime = Stopwatch.StartNew();
            while( socket.Poll( 1, SelectMode.SelectWrite ) == false )
            {
                Sleep();
                if( runTime.ElapsedMilliseconds > TimeOut )
                {
                    throw new TimeoutException( $"{Type} Time Out on send!!! " );
                }
            }
            socket.Send( sendMsg  );
            Log.Info("SendMessageBySocket Ended");
        }

        private void CreateMainSocket()
        {
            _mainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _mainSocket.SendTimeout = 
                _mainSocket.ReceiveTimeout = (int)TimeSpan.FromMinutes(1).TotalMilliseconds;
            _mainSocket.Blocking = true;
            _mainSocket.ReceiveBufferSize = _mainSocket.SendBufferSize = int.MaxValue;
        }

        /// <summary>
        /// Creates the client socket.
        /// </summary>
        private void CreateClientSocket()
        {
            try
            {
                CreateMainSocket();
            }
            catch (Exception error)
            {
                OnError(error.Message, error);
                return;
            }

            OnSocketCreate();

            try
            {
                while( true )
                {
                    _mainSocket.Connect( GetIPEndPoint() );
                    if( _mainSocket.Connected )
                        IsConnected = true;
                    break;
                }
                OnConnected();
            }
            catch( SocketException error )
            {
                OnError(error.Message, error);
            }
        }



        /// <summary>
        /// Creates the server socket.
        /// </summary>
        private void CreateServerSocket()
        {
            try
            {
                CreateMainSocket();

            }
            catch( SocketException error )
            {
                OnError( error.Message, error );
                return;
            }

            OnSocketCreate();

            try
            {
                _mainSocket.Bind( GetIPEndPoint() );
                _mainSocket.Listen( 1 );
                while( true )
                {
                    _handler = _mainSocket.Accept();
                    if( _handler.Connected )
                        IsConnected = true;
                    break;
                }
                OnConnected();
            }
            catch( Exception exception )
            {
                OnError( exception.Message, exception );
            }
        }

        /// <summary>
        /// Gets the ip end point.
        /// </summary>
        /// <returns></returns>
        private IPEndPoint GetIPEndPoint()
        {
            if( IPAddress.TryParse( Host, out _ipAddr ) == false )
            {
                _ipHost = Dns.GetHostEntry(Host);
                _ipAddr = _ipHost.AddressList.First(
                        x => x.IsIPv4MappedToIPv6 == false && x.IsIPv6LinkLocal == false && x.IsIPv6Multicast == false &&
                            x.IsIPv6SiteLocal == false && x.IsIPv6Teredo == false);
            }

            return new IPEndPoint( _ipAddr, Port );
        }

        /// <summary>
        /// Sleeps this instance.
        /// </summary>
        private void Sleep()
        {
            Thread.Sleep( SleepTime );
        }

        #region Events

        public event  SocketCreatedHandler Created;

        protected void OnSocketCreate()
        {
            Created?.Invoke();
        }

        public event ConnectedHandler Connected;

        protected void OnConnected()
        {
            Connected?.Invoke();
        }

        public event ErrorHandler Error;

        protected void OnError(string message, Exception exception)
        {
            Log.Error(message, exception);
            Error?.Invoke( this, new ErrorEventArgs( message ) );
        }

        #endregion

        #region Delegations

        public delegate void SocketCreatedHandler();

        public delegate void ConnectedHandler();

        public delegate void ErrorHandler(object seneder, ErrorEventArgs args);

        #endregion
        
        public void Dispose()
        {
            Dispose( true );
        }

        protected virtual void Dispose(bool isDispose)
        {
            if( _isDisposed )
            {
                return;
            }


            if( isDispose )
            {
                _mainSocket.Dispose();
                _handler.Dispose();
            }

            _isDisposed = true;
        }
    }
}