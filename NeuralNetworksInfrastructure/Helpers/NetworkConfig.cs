﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using NeuralNetworksInfrastructure.Enum;

namespace NeuralNetworksInfrastructure.Helpers
{
    public class NetworkConfig:INotifyPropertyChanged
    {
        private ETypesOfTPM _valueType;
        private int _k;
        private int _l;
        private int _n;

        /// <summary>
        /// The separator
        /// </summary>
        private const char Separator = ';';

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkConfig"/> class.
        /// </summary>
        public NetworkConfig()
        {
            N = 1;
            K = 1;
            L = 1;
            ValueType = ETypesOfTPM.Int;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkConfig"/> class.
        /// </summary>
        /// <param name="n">The n.</param>
        /// <param name="k">The k.</param>
        /// <param name="l">The l.</param>
        /// <param name="type">The type.</param>
        public NetworkConfig(int n, int k, int l, ETypesOfTPM type)
        {
            N = n;
            K = k;
            L = l;
            ValueType = type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkConfig"/> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public NetworkConfig(string config)
        {
            if (String.IsNullOrEmpty(config)) return;
            string str;
            N = GetParam(config, out str);
            K = GetParam(str, out str);
            L = GetParam(str, out str);
            ETypesOfTPM.TryParse( GetParam(str, out str).ToString() ,out _valueType) ;
        }

        /// <summary>
        ///     Count of perceptron nodes
        /// </summary>
        public int N
        {
            get { return _n; }
            set
            {
                if( value == _n ) return;
                _n = value;
                OnPropertyChanged();
                OnPropertyChanged( nameof( IsConfigurationCompleted ) );
            }
        }

        /// <summary>
        ///     Weight limits {-L,...0,...,+L}
        /// </summary>
        public int L
        {
            get { return _l; }
            set
            {
                if( value == _l ) return;
                _l = value;
                OnPropertyChanged();
                OnPropertyChanged( nameof( IsConfigurationCompleted ) );
            }
        }

        /// <summary>
        ///     Count of perseptones
        /// </summary>
        public int K
        {
            get { return _k; }
            set
            {
                if( value == _k ) return;
                _k = value;
                OnPropertyChanged();
                OnPropertyChanged( nameof( IsConfigurationCompleted ) );
            }
        }

        /// <summary>
        /// Gets or sets the type of the value.
        /// </summary>
        /// <value>
        /// The type of the value.
        /// </value>
        public ETypesOfTPM? ValueType
        {
            get { return _valueType; }
            set
            {
                if( value == _valueType ) return;
                if(value.HasValue==false) return;
                _valueType = value.Value;
                OnPropertyChanged();
                OnPropertyChanged( nameof( IsConfigurationCompleted ) );
            }
        }


        public int CountOfPerceptrones => K * N;

        /// <summary>
        /// Gets a value indicating whether this instance is configuration completed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is configuration completed; otherwise, <c>false</c>.
        /// </value>
        public bool IsConfigurationCompleted
        {
            get { return N != 0 && K != 0 && L != 0 && ValueType.HasValue; }
        }

        /// <summary>
        /// Gets the parameter.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        private int GetParam(string config, out string str)
        {
            var indexOfSeparator = config.IndexOf(Separator);
            var value = config.Substring(0, indexOfSeparator);
            int val;
            int.TryParse(value, out val);
            str = config.Remove(0, indexOfSeparator + 1);
            return val;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <param name="isDescription">if set to <c>true</c> [is description].</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public string ToString(bool isDescription)
        {
            return String.Format(isDescription ? "N={0}{4}K={1}{4}L={2}{4}Type={3}{4}" : "{0}{4}{1}{4}{2}{4}{3}{4}", N, K, L, (int)ValueType, Separator);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }
    }
}