﻿using System.Collections.Generic;

namespace NeuralNetworksInfrastructure.Helpers
{
    public class CompareOutputsInfo
    {
        /// <summary>
        /// Gets or sets the main output.
        /// </summary>
        /// <value>
        /// The main output.
        /// </value>
        public string MainOutput { get; set; }
        /// <summary>
        /// Gets or sets the handler output.
        /// </summary>
        /// <value>
        /// The handler output.
        /// </value>
        public string HandlerOutput { get; set; }

        /// <summary>
        /// Gets or sets the inputs.
        /// </summary>
        /// <value>
        /// The inputs.
        /// </value>
        public IEnumerable<IEnumerable<string>> Inputs { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is outputs equals.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is outputs equals; otherwise, <c>false</c>.
        /// </value>
        public bool IsOutputsEquals => MainOutput.Equals( HandlerOutput );
    }
    
}