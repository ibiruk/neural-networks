﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NeuralNetworksInfrastructure.Helpers
{
    public static class Extensions
    {
        
        /// <summary>
        /// Gets the inputs from string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<string>> GetInputsFromString(this string str)
        {
            return str.Split( '#' ).Select( x => x.Split( '|' ) );
        }

        /// <summary>
        /// Inputs to string.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static string InputsToString(this IEnumerable<IEnumerable<string>> collection)
        {
            return string.Join( "#", collection.Select( x => string.Join( "|", x ) ) );
        }

        /// <summary>
        /// Determines whether [is null or empty].
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string str)
        {
            return String.IsNullOrEmpty( str );
        }

        /// <summary>
        /// Parses the enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
       /// <returns></returns>
        public static T ParseEnum<T>(this string value)
        {
            return (T)System.Enum.Parse(typeof(T), value, true);
        }
    }
}