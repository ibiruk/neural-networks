﻿using System.Collections;

namespace NeuralNetworksInfrastructure.Helpers
{
    public class SimpleTokenizerHelper
    {
        public const char QuoteChar = '\'';
        public const char ArgSeparator = ';';

        private IEnumerator strEnumerator;

        public SimpleTokenizerHelper(string str)
        {
            Initialize( str );
        }

        private void Initialize(string str)
        {
            var prepared = str.Replace(" ", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
            strEnumerator =  prepared.Split( ArgSeparator ).GetEnumerator();
        }

        public string NextTokenRequired()
        {
            strEnumerator.MoveNext();
            return strEnumerator.Current.ToString();
        }


    }
}