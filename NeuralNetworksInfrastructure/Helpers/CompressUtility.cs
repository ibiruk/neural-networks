﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zlib;

namespace NeuralNetworksInfrastructure.Helpers
{
    public class CompressUtility
    {
        public static async Task<byte[]> CompressAsync(string str)
        {
            using (var output = new MemoryStream())
            {
                using (var gzip = new DeflateStream(output, CompressionMode.Compress))
                {
                    using (var writer = new StreamWriter(gzip, GetEncoder()))
                    {
                        await writer.WriteAsync(str);
                    }
                }

                return output.ToArray();
            }
        }

        public static async Task<string> DecompressAsync(byte[] bytes)
        {
            using (var inputStream = new MemoryStream(bytes))
            {
                using (var gzip = new DeflateStream(inputStream, CompressionMode.Decompress))
                {
                    using (var reader = new StreamReader(gzip, GetEncoder()))
                    {
                        return await reader.ReadToEndAsync();
                    }
                }
            }
        }

        public static Encoding GetEncoder()
        {
            return Encoding.Unicode;
        }
    }
}