using System;
using System.Collections.Generic;
using NeuralNetworksInfrastructure.Reports;

namespace NeuralNetworksInfrastructure.Interfaces
{
    public interface IResearch 
    {
        int NumberOfTests { get; }
        int LimitOfAttempts { get;  }
        List<Report> Reports { get;} 
    }
}