﻿namespace NeuralNetworksInfrastructure.Interfaces
{
    public interface INumberSystem<out T>: INumberSystemBase
    {
        INumberSystem<T> GetIdentity();
    }

    
}
