using System.Collections.Generic;
using JetBrains.Annotations;
using NeuralNetworksInfrastructure.Helpers;

namespace NeuralNetworksInfrastructure.Interfaces
{
    public interface ITPM
    {
        string Output { get; }

        IEnumerable<IEnumerable<string>> GenerateInput(NetworkConfig config);
        NetworkConfig GetNetworkConfiguration();
        string GetOutput(IEnumerable<IEnumerable<string>> aInputs);
        void Synchronize();
        IEnumerable<IEnumerable<string>> GetTPMWeights();

        string GetWeightsStrings();
    }
}