﻿using System.Collections.Generic;

namespace NeuralNetworksInfrastructure.Interfaces
{
    public interface INumberSystemBase
    {
        bool IsIdentity { get; }
        double Real { get; set; }

        IEnumerable<double> GetComponents();
    }
}