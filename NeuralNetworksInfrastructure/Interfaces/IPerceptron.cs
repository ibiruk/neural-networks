using System.Collections.Generic;
using NeuralNetworksInfrastructure.Helpers;

namespace NeuralNetworksInfrastructure.Interfaces
{
    public interface IPerceptron
    {
        IPerceptron CreatePerceptron(NetworkConfig config);

        string GetOutput(IEnumerable<string> aInputs);
        string GetMinOutput();
        string Multiply(string input, string weight);
        string GetWeight(int index);
        string Sigma(string value);
        void AktualizeWeights(string output);
        string GetRandomValue();
        string GetRandomInput();
    }
}