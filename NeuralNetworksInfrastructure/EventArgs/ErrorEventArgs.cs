﻿namespace NeuralNetworksInfrastructure.EventArgs
{
    public class ErrorEventArgs : System.EventArgs
    {
        private readonly string _message;

        public ErrorEventArgs(string message)
        {
            _message = message;
        }

        public string ErrorMessage
        {
            get { return _message; }
        }
    }
}