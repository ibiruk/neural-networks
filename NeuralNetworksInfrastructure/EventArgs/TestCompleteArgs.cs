﻿namespace NeuralNetworksInfrastructure.EventArgs
{
    public class TestCompleteArgs : System.EventArgs
    {
        private readonly int _testCount;
        private readonly int _testNumber;

        public TestCompleteArgs(int testCount, int testNumber)
        {
            _testCount = testCount;
            _testNumber = testNumber;
        }

        public int TestCount
        {
            get { return _testCount; }
        }

        public int TestNumber
        {
            get { return _testNumber; }
        }
    }
}