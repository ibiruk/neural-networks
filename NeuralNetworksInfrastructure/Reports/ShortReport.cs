﻿using System;
using System.Collections.Generic;
using System.Linq;
using NeuralNetworksInfrastructure.Interfaces;

namespace NeuralNetworksInfrastructure.Reports
{
    public class ShortReport
    {
        public int CountOfTests { get; set; }

        public int SyncCount { get; set; }

        public int LimitOfAttempts { get; set; }

        public int AveragePreSyncNumberOfMaches { get; set; }

        public double PercentPreSyncNumberOfMaches { get; set; }

        public int AveragePreSyncMaches { get; set; }

        public double PercentPreSyncMaches { get; set; }

        public decimal AverageElipsatedTime { get; set; }

        public double PercentElipsatedTime { get; set; }

        public int MinMaches { get; set; }

        public int MaxMaches { get; set; }

        public int MinPreSyncMaches { get; set; }

        public int MaxPreSyncMaches { get; set; }

        public List<Segment> AllMaches { get; set; }

        public List<Segment> PreSyncMaches { get; set; }

        public static ShortReport GetShortReport(IResearch research)
        {
            var shortReport = new ShortReport();
            shortReport.CountOfTests = research.NumberOfTests;
            shortReport.LimitOfAttempts = research.LimitOfAttempts;
            var succeed = research.Reports.FindAll(x => x.IsSyncSucceed);
            shortReport.SyncCount = succeed.Count;
            GetAverageInfo(shortReport, succeed);
            GetMinMaxInfo(shortReport, succeed);
            GetSegments(shortReport, succeed);
            GetSegmentsPreSync(shortReport, succeed);
            return shortReport;
        }

        private static void GetAverageInfo(ShortReport shortReport, List<Report> succeed)
        {
            int preSyncNumberOfMaches = 0, preSyncMaches = 0;
            var elipsatedTime = decimal.Zero;
            foreach (var report in succeed)
            {
                preSyncNumberOfMaches += report.TestReports.Count(x => x.IsOutputMach);
                for (var i = report.TestReports.Count - 1; i > 0; i--)
                {
                    if (!report.TestReports[i].IsOutputMach)
                        break;
                    preSyncMaches++;
                }
                elipsatedTime += report.ElipsatedTime;
            }

            var averagePreSyncMaches = Math.Round( preSyncMaches/(double)shortReport.SyncCount + 0.5 );
            var averagePreSyncNumberOfMaches = Math.Round(preSyncNumberOfMaches / (double)shortReport.SyncCount + 0.5);
            shortReport.AveragePreSyncMaches = Convert.ToInt32( !double.IsNaN( averagePreSyncMaches ) ? averagePreSyncMaches : 0 );
            shortReport.AveragePreSyncNumberOfMaches = Convert.ToInt32(!double.IsNaN(averagePreSyncNumberOfMaches) ? averagePreSyncMaches : 0);
            shortReport.AverageElipsatedTime = elipsatedTime/shortReport.SyncCount;

            int countPreSync = 0, countPreSyncNumber = 0, countElipsated = 0;
            foreach (var report in succeed)
            {
                if (report.ElipsatedTime <= shortReport.AverageElipsatedTime)
                {
                    countElipsated++;
                }
                if (report.TestReports.Count(x => x.IsOutputMach) <= shortReport.AveragePreSyncNumberOfMaches)
                {
                    countPreSyncNumber++;
                }
                var curPreSyncMaches = 0;
                for (var i = report.TestReports.Count - 1; i > 0; i--)
                {
                    if (!report.TestReports[i].IsOutputMach)
                        break;
                    curPreSyncMaches++;
                }
                if (curPreSyncMaches <= shortReport.AveragePreSyncMaches)
                {
                    countPreSync++;
                }
            }
            shortReport.PercentPreSyncMaches = Math.Round(countPreSync/(double) succeed.Count, 2)*100;
            shortReport.PercentPreSyncNumberOfMaches = Math.Round(countPreSyncNumber/(double) succeed.Count, 2)*100;
            shortReport.PercentElipsatedTime = Math.Round(countElipsated/(double) succeed.Count, 2)*100;
        }

        private static void GetMinMaxInfo(ShortReport shortReport, List<Report> succeed)
        {
            shortReport.MaxMaches = int.MinValue;
            shortReport.MaxPreSyncMaches = int.MinValue;
            shortReport.MinMaches = int.MaxValue;
            shortReport.MinPreSyncMaches = int.MaxValue;
            foreach (var report in succeed)
            {
                if (shortReport.MaxMaches < report.TestReports.Count(x => x.IsOutputMach))
                {
                    shortReport.MaxMaches = report.TestReports.Count(x => x.IsOutputMach);
                }
                if (shortReport.MinMaches > report.TestReports.Count(x => x.IsOutputMach))
                {
                    shortReport.MinMaches = report.TestReports.Count(x => x.IsOutputMach);
                }
                var curPreSyncMaches = 0;
                for (var i = report.TestReports.Count - 1; i > 0; i--)
                {
                    if (!report.TestReports[i].IsOutputMach)
                        break;
                    curPreSyncMaches++;
                }
                if (shortReport.MinPreSyncMaches > curPreSyncMaches)
                {
                    shortReport.MinPreSyncMaches = curPreSyncMaches;
                }
                if (shortReport.MaxPreSyncMaches < curPreSyncMaches)
                {
                    shortReport.MaxPreSyncMaches = curPreSyncMaches;
                }
            }
        }

        private static void GetSegments(ShortReport shortReport, List<Report> succeed)
        {
            var segments = new List<Segment>();
            var step = Convert.ToInt32((shortReport.MaxMaches - shortReport.MinMaches)/40);
            for (var i = 0; i < 40; i++)
            {
                var segment = new Segment();
                segment.MinValue = shortReport.MinMaches + step*i;
                segment.MaxValue = shortReport.MinMaches + step*(i + 1);
                segment.AverageValue = segment.MinValue + step/2;
                segments.Add(segment);
            }
            foreach (var segment in segments)
            {
                segment.Count =
                    succeed.Select(report => report.TestReports.Count(x => x.IsOutputMach)).Count(
                        maches => maches >= segment.MinValue && maches < segment.MaxValue);
            }
            shortReport.AllMaches = segments.ToList();
        }

        private static void GetSegmentsPreSync(ShortReport shortReport, List<Report> succeed)
        {
            var segments = new List<Segment>();
            var step = Convert.ToInt32((shortReport.MaxPreSyncMaches - shortReport.MinPreSyncMaches)/40);
            for (var i = 0; i < 40; i++)
            {
                var segment = new Segment();
                segment.MinValue = shortReport.MinPreSyncMaches + step*i;
                segment.MaxValue = shortReport.MinPreSyncMaches + step*(i + 1);
                segment.AverageValue = segment.MinValue + step/2;
                segments.Add(segment);
            }
            foreach (var segment in segments)
            {
                var count = 0;
                foreach (var report in succeed)
                {
                    var curPreSyncMaches = 0;
                    for (var i = report.TestReports.Count - 1; i > 0; i--)
                    {
                        if (!report.TestReports[i].IsOutputMach)
                            break;
                        curPreSyncMaches++;
                    }
                    if (curPreSyncMaches >= segment.MinValue && curPreSyncMaches < segment.MaxValue)
                    {
                        count++;
                    }
                }
                segment.Count = count;
            }
            shortReport.PreSyncMaches = segments.ToList();
        }
    }
}