﻿namespace NeuralNetworksInfrastructure.Reports
{
    public class Segment
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }

        public int AverageValue { get; set; }
        public int Count { get; set; }
    }
}