﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NeuralNetworksInfrastructure.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class Report
    {
        private TimeSpan _t1;


        /// <summary>
        /// Initializes a new instance of the <see cref="Report"/> class.
        /// </summary>
        public Report()
        {
            TestReports = new List<TestReport>();
        }

        /// <summary>
        /// Gets or sets the number of test.
        /// </summary>
        /// <value>
        /// The number of test.
        /// </value>
        public int NumberOfTest { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is synchronize succeed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is synchronize succeed; otherwise, <c>false</c>.
        /// </value>
        public bool IsSyncSucceed { get; set; }


        /// <summary>
        /// Gets or sets the test reports.
        /// </summary>
        /// <value>
        /// The test reports.
        /// </value>
        public List<TestReport> TestReports { get; set; }


        /// <summary>
        /// Gets or sets the ellapsed stopwatch.
        /// </summary>
        /// <value>
        /// The ellapsed stopwatch.
        /// </value>
        [XmlIgnore]
        [JsonIgnore]
        public Stopwatch EllapsedStopwatch { get; set; }

        /// <summary>
        /// Gets or sets the elipsated time.
        /// </summary>
        /// <value>
        /// The elipsated time.
        /// </value>
        public decimal ElipsatedTime
        {
            get { return (decimal) EllapsedStopwatch.ElapsedTicks/Stopwatch.Frequency; }
            set { _t1 = new TimeSpan((long) value*Stopwatch.Frequency); }
        }
    }
}