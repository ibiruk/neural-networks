﻿using System;
using System.Diagnostics;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NeuralNetworksInfrastructure.Reports
{
    public sealed class TestReport
    {
        private TimeSpan _t1;

        [XmlIgnore]
        [JsonIgnore]
        public Stopwatch StopwatchSendMessage { get; set; }

        [XmlIgnore]
        [JsonIgnore]
        public Stopwatch StopwatchReciveMessage { get; set; }

        [XmlIgnore]
        [JsonIgnore]
        public Stopwatch StopwatchCalculateWeight { get; set; }

        public int NumberOfTest { get; set; }

        public decimal TimeToSendMessage
        {
            get
            {
                return StopwatchSendMessage == null
                    ? decimal.Zero
                    : (decimal) StopwatchSendMessage.ElapsedTicks/Stopwatch.Frequency;
            }
            set { _t1 = new TimeSpan((long) value*Stopwatch.Frequency); }
        }

        public decimal TimeToReciveMessage
        {
            get
            {
                return StopwatchReciveMessage == null
                    ? decimal.Zero
                    : (decimal) StopwatchReciveMessage.ElapsedTicks/Stopwatch.Frequency;
            }
            set { _t1 = new TimeSpan((long) value*Stopwatch.Frequency); }
        }

        public decimal TimeToCalculateWeight
        {
            get
            {
                return StopwatchCalculateWeight == null
                    ? decimal.Zero
                    : (decimal) StopwatchCalculateWeight.ElapsedTicks/Stopwatch.Frequency;
            }
            set { _t1 = new TimeSpan((long) value*Stopwatch.Frequency); }
        }

        public bool IsOutputMach { get; set; }

        public decimal ElipsatedTime
        {
            get { return TimeToSendMessage + TimeToSendMessage + TimeToCalculateWeight; }
            set { _t1 = new TimeSpan((long) value*Stopwatch.Frequency); }
        }

        public bool IsWeightMach { get; set; }
    }
}